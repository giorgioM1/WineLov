import random
import datetime


from influx_client import InfluxConnection
import time


class InfluxRest:
    def __init__(self):
        self.conn = InfluxConnection().endPointConnection()

    def addPoint(self):
        t0 = datetime.datetime.utcnow()
        json_body = [
            {
                "measurement": "ratings",
                "tags": {
                    "user": 1,
                    "wine_id": 1,
                    "name": 'ciaone'
                },
                "time": t0,
                "fields": {
                    "rating": random.randint(1, 5)
                }
            }
        ]

        x = self.conn.write_points(json_body)
        if x:
            print 'save'
        else:
            print 'dont save'

        result = self.conn.query('select rating from ratings;')
        print result


if __name__ == '__main__':
    conn = InfluxRest()
    #conn.addPoint()
