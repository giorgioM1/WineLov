import json
import string

pathDB = "/home/giorgio/Scrivania/WineLov/DB/"


def word_count(str):
    counts = dict()
    words = str.split()

    for word in words:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1

    return counts


def getWordCount(pathfile):
    files = open(pathfile, mode='r').read()
    js = json.loads(files)
    row = ''

    for i in js:
        des = i['description']
        if des is not None:
            for p in string.punctuation:
                des = des.replace(p,' ')
                des = des.lower()
            row += des

    return word_count(row)


def main():
    wc = getWordCount(pathDB + 'DBRed.json')
    for w in wc:
        print w


if __name__ == '__main__':
    main()
