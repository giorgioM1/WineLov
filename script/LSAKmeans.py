import pandas as pd
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn.cluster import KMeans

from App.MultiCategoryClassification import tokenize_and_stem_and_stopword
from App.similarityDesc import pathCSV


def main():
    header = ['id', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']
    wines = pd.read_csv(pathCSV + "DB.csv", sep=',', names=header, low_memory=False)


    vectorizer = TfidfVectorizer(max_df=0.7, min_df=100, max_features=30000, stop_words='english', use_idf=True,
                                 tokenizer=tokenize_and_stem_and_stopword, ngram_range=(1, 3), strip_accents='unicode')

    tfidf_Matrix = vectorizer.fit_transform(wines.description)
    print tfidf_Matrix.shape
    print tfidf_Matrix

    vocabulary = vectorizer.vocabulary_
    idf = vectorizer.idf_
    print len(idf), len(vocabulary)
    print idf
    df = pd.DataFrame(vocabulary.items(), columns=['word', 'reps'])
    dfw = pd.DataFrame(idf, columns=['weight'])
    df = pd.concat([df, dfw], axis=1)
    df = df.sort_values(['weight'], ascending=False)
    print df




    svd = TruncatedSVD(100)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    newBaseLSA = lsa.fit_transform(tfidf_Matrix)
    explained_variance = svd.explained_variance_ratio_.sum()
    print("Explained variance of the SVD step: {}%".format(
        int(explained_variance * 100)))


if __name__ == '__main__':
    main()
