import pandas as pd
import matplotlib.pyplot as plt;
from sklearn.metrics import confusion_matrix
import itertools

from sklearn.model_selection import cross_val_score

from createDB import pathCSV
import numpy as np
import matplotlib.pyplot as plt
from App.foodClassifier import LabelClassification

pathChart = "/home/giorgio/Scrivania/WineLov/chart/"

categoryfood = ['crudi', 'crostacei', 'salmone', 'formaggio pastamolle', 'formaggio caprino', 'formaggio stagionato',
                'formaggio mediostagionato', 'formaggio erbolinati', 'pesce magro', 'carni bianche', 'carni rosse',
                'cacciagione', 'funghi e tartufi', 'salumi', 'legumi', 'pasta', 'marinati', 'speziati', 'riso']


def OneVSAllPredict(LC_Food, des):
    response = []
    for i in range(0, len(categoryfood)):
        p = LC_Food[i].predict(des)
        if p is True:
            response.append(categoryfood[i])
    return response


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig(pathChart + 'CM' + str(classes[1]) + '.png')
    plt.cla()
    plt.clf()
    plt.close()


def updateVariable(p, LC):
    truepositive = 0
    falsepositive = 0
    truenegative = 0
    falsenegative = 0
    numRight = 0

    for i in range(0, len(p)):
        # print LC.labelTest[i]
        if p[i] == True and LC.labelTest[i]:
            truepositive += 1
        elif p[i] == True and not LC.labelTest[i]:
            falsenegative += 1
        elif p[i] == False and not LC.labelTest[i]:
            truenegative += 1
        elif p[i] == False and LC.labelTest[i]:
            falsepositive += 1
        if p[i] == LC.labelTest[i]:
            numRight += 1
    print falsenegative, falsepositive, truepositive, truenegative
    confMatrix = confusion_matrix(LC.labelTest, p)
    print confMatrix
    print("  (%d / %d) correct - %.2f%%" % (
        numRight, len(LC.labelTest), float(numRight) / float(len(LC.labelTest)) * 100.0))

    return truepositive, truenegative, falsepositive, falsenegative, numRight, confMatrix


def main():
    header = ['id_wine', 'name', 'description', 'variety', 'food_matching']
    df = pd.read_csv(pathCSV + 'matchingFood.csv', sep=',', header=None, names=header)
    wines = df[pd.notnull(df['food_matching'])]
    # print wines.food_matching
    response = []
    LC_Food = []
    accuracy = []
    recall = []
    precision = []
    Fmeasure = []
    accuracySVM = []
    recallSVM = []
    precisionSVM = []
    FmeasureSVM = []
    for i in range(0, len(categoryfood)):
        LC = LabelClassification(wines, str(categoryfood[i]), 0.95)
        LC_Food.append(LC)
        # print LC.labelTest

        print("  %d training examples (%d %s)" % (len(LC.labelTrain), sum(LC.labelTrain), categoryfood[i]))
        print("  %d test examples (%d %s)" % (len(LC.labelTest), sum(LC.labelTest), categoryfood[i]))

        X_test_tfidf = LC.vectorized.transform(LC.testSet)
        X_test_lsa = LC.lsa.transform(X_test_tfidf)
        p = LC.classifier.predict(X_test_lsa)
        psvm = LC.svm.predict(X_test_lsa)
        print psvm

        # Measure accuracy
        # K-nn statistics
        truepositive, truenegative, falsepositive, falsenegative, numRight, ConfusionMatrixKNN = updateVariable(p, LC)
        plot_confusion_matrix(ConfusionMatrixKNN, ['Altro', categoryfood[i]], )
        accuracy.append(float(numRight) / float(len(LC.labelTest)))
        recallLC = float(truepositive) / float(truepositive + falsenegative)
        precisionLC = float(truepositive) / float(truepositive + falsepositive)
        negativePrecision = float(truenegative)/float(truenegative + falsenegative)
        print "negative precision: " +str (negativePrecision)
        precision.append(precisionLC)
        recall.append(recallLC)
        FMeasureLC = 2 * (float(precisionLC * recallLC) / (float(precisionLC + recallLC)))
        Fmeasure.append(FMeasureLC)

        # SVM statistics
        truepositive, truenegative, falsepositive, falsenegative, numRight, ConfusionMatrixSVM = updateVariable(psvm, LC)
        if truepositive is not 0:
            accuracySVM.append(float(numRight) / float(len(LC.labelTest)))
            recallLC = float(truepositive) / float(truepositive + falsenegative)
            precisionLC = float(truepositive) / float(truepositive + falsepositive)
            precisionSVM.append(precisionLC)
            recallSVM.append(recallLC)
            FMeasureLC = 2 * (float(precisionLC * recallLC) / (float(precisionLC + recallLC)))
            FmeasureSVM.append(FMeasureLC)
        else:
            accuracySVM.append(float(numRight) / float(len(LC.labelTest)))
            recallLC = 0
            precisionLC = 0
            precisionSVM.append(precisionLC)
            recallSVM.append(recallLC)
            FmeasureSVM.append(0)

    print "K-NN"
    print accuracy, np.mean(accuracy)
    print recall, np.mean(recall)
    print precision, np.mean(precision)
    print Fmeasure, np.mean(Fmeasure)

    print "SVM"
    print accuracySVM, np.mean(accuracySVM)
    print recallSVM, np.mean(recallSVM)
    print precisionSVM, np.mean(precisionSVM)
    print FmeasureSVM, np.mean(FmeasureSVM)

    n_groups = np.arange(len(categoryfood))

    figA, ax = plt.subplots()

    index = np.arange(19)
    bar_width = 0.20

    opacity = 0.65
    error_config = {'ecolor': '0.3'}

    rects1 = ax.bar(index, accuracy, bar_width,
                    alpha=opacity, color='r',
                    error_kw=error_config,
                    label='Accuracy')

    rects2 = ax.bar(index + bar_width, precision, bar_width,
                    alpha=opacity, color='b',
                    error_kw=error_config,
                    label='Precision')

    rects3 = ax.bar(index + 2 * bar_width, recall, bar_width,
                    alpha=opacity, color='y',
                    error_kw=error_config,
                    label='Recall')

    rects4 = ax.bar(index + 3 * bar_width, Fmeasure, bar_width,
                    alpha=opacity, color='c',
                    error_kw=error_config,
                    label='F1-score')

    ax.set_xlabel('Categories')
    ax.set_ylabel('Scores')
    ax.set_title('Metrics for evaluation the model "Food Matching"')
    ax.set_xticks(index + bar_width / 2)
    ax.set_xticklabels(categoryfood)
    ax.legend()
    figA = plt.gcf()
    figA.tight_layout()
    figA.autofmt_xdate()
    plt.savefig(pathChart + 'FMKNN' + '.png')

    # SVM
    figS, axS = plt.subplots()

    index = np.arange(19)
    bar_width = 0.20

    opacity = 0.65
    error_config = {'ecolor': '0.3'}

    rects1 = axS.bar(index, accuracySVM, bar_width,
                     alpha=opacity, color='r',
                     error_kw=error_config,
                     label='Accuracy')

    rects2 = axS.bar(index + bar_width, precisionSVM, bar_width,
                     alpha=opacity, color='b',
                     error_kw=error_config,
                     label='Precision')

    rects3 = axS.bar(index + 2 * bar_width, recallSVM, bar_width,
                     alpha=opacity, color='y',
                     error_kw=error_config,
                     label='Recall')

    rects4 = axS.bar(index + 3 * bar_width, FmeasureSVM, bar_width,
                     alpha=opacity, color='c',
                     error_kw=error_config,
                     label='F1-score')

    axS.set_xlabel('Categories')
    axS.set_ylabel('Scores')
    axS.set_title('Metrics for evaluation the model "Food Matching"')
    axS.set_xticks(index + bar_width / 2)
    axS.set_xticklabels(categoryfood)
    axS.legend()
    figS = plt.gcf()
    figS.tight_layout()
    figS.autofmt_xdate()
    plt.savefig(pathChart + 'FMSVM' + '.png')

    # Compare Values
    figAc, axA = plt.subplots()

    index = np.arange(19)
    bar_width = 0.30

    opacity = 0.65
    error_config = {'ecolor': '0.3'}

    rects1 = axA.bar(index, accuracy, bar_width,
                     alpha=opacity, color='r',
                     error_kw=error_config,
                     label='KNN')

    rects2 = axA.bar(index + bar_width, accuracySVM, bar_width,
                     alpha=opacity, color='b',
                     error_kw=error_config,
                     label='SVM')

    axA.set_xlabel('Categories')
    axA.set_ylabel('Accuracy')
    axA.set_title('Metrics for evaluation the model "Food Matching"')
    axA.set_xticks(index + bar_width / 2)
    axA.set_xticklabels(categoryfood)
    axA.legend()
    figAc = plt.gcf()
    figAc.tight_layout()
    figAc.autofmt_xdate()
    plt.savefig(pathChart + 'FMAccuracy' + '.png')

    # PRecision

    # Compare Values
    figP, axP = plt.subplots()

    index = np.arange(19)
    bar_width = 0.30

    opacity = 0.65
    error_config = {'ecolor': '0.3'}

    rects1 = axP.bar(index, accuracy, bar_width,
                     alpha=opacity, color='r',
                     error_kw=error_config,
                     label='KNN')

    rects2 = axP.bar(index + bar_width, accuracySVM, bar_width,
                     alpha=opacity, color='b',
                     error_kw=error_config,
                     label='SVM')

    axP.set_xlabel('Categories')
    axP.set_ylabel('Precision')
    axP.set_title('Metrics for evaluation the model "Food Matching"')
    axP.set_xticks(index + bar_width / 2)
    axP.set_xticklabels(categoryfood)
    axP.legend()
    figP = plt.gcf()
    figP.tight_layout()
    figP.autofmt_xdate()
    plt.savefig(pathChart + 'FMPrecision' + '.png')
    # Recall
    figP, axRecall = plt.subplots()

    index = np.arange(19)
    bar_width = 0.30

    opacity = 0.65
    error_config = {'ecolor': '0.3'}

    rects1 = axRecall.bar(index, recall, bar_width,
                          alpha=opacity, color='r',
                          error_kw=error_config,
                          label='KNN')

    rects2 = axRecall.bar(index + bar_width, recallSVM, bar_width,
                          alpha=opacity, color='b',
                          error_kw=error_config,
                          label='SVM')

    axRecall.set_xlabel('Categories')
    axRecall.set_ylabel('Recall')
    axRecall.set_title('Metrics for evaluation the model "Food Matching"')
    axRecall.set_xticks(index + bar_width / 2)
    axRecall.set_xticklabels(categoryfood)
    axRecall.legend()
    figP = plt.gcf()
    figP.tight_layout()
    figP.autofmt_xdate()
    plt.savefig(pathChart + 'FMPrecision' + '.png')

    # Accuracy

    figP, axacc = plt.subplots()

    index = np.arange(19)
    bar_width = 0.35

    opacity = 0.65
    error_config = {'ecolor': '0.3'}

    rects1 = axacc.bar(index, accuracy, bar_width,
                       alpha=opacity, color='r',
                       error_kw=error_config,
                       label='Accuracy KNN')

    axacc.set_xlabel('Categories')
    axacc.set_ylabel('Scores')
    axacc.set_title('Metrics for evaluation the model "Food Matching"')
    axacc.set_xticks(index + bar_width / 2)
    axacc.set_xticklabels(categoryfood)
    axacc.legend()
    figP = plt.gcf()
    figP.tight_layout()
    figP.autofmt_xdate()
    plt.savefig(pathChart + 'Accuracy' + '.png')

    # #multiple classification
    # FM = oneVSAllClassifier()
    #
    # while True:
    #     try:
    #         des = input("new wine description: ")
    #         # for i in range(0, len(categoryfood)):
    #         response = FM.oneVSallPredict(des)
    #         # p = LC_Food[i].predict(des)
    #         #    if p is True:
    #         #        response.append(categoryfood[i])
    #         print response
    #     except ValueError:
    #         print "you have insert a incorrect value into new description"
    #         continue
    # p = LC.classifier.predict(X_test_lsa)
    # # Measure accuracy
    # numRight = 0
    # for i in range(0, len(p)):
    #     if p[i] == LC.labelTest[i]:
    #         numRight += 1
    #
    # print("  (%d / %d) correct - %.2f%%" % (
    #     numRight, len(LC.labelTest), float(numRight) / float(len(LC.labelTest)) * 100.0))


if __name__ == '__main__':
    main()
