import numpy as np
import matplotlib.pyplot as plt;
import pandas as pd
from surprise import Reader, Dataset, SVD, evaluate
from surprise.model_selection import cross_validate

from App.CollaborativeFilter import pathCSV
from classifyFoodWine import pathChart

plt.rcdefaults()

if __name__ == '__main__':
    x = [0.78150077, 0.82611034, 0.87077924, 0.8793949, 0.88058985, 0.88090333,
         0.88504844, 0.89050771, 0.8906719, 0.90541002, 0.91670659, 0.91928215,
         0.92256908, 0.92625237, 0.92764195, 0.93076866, 0.94210036, 0.9452318,
         0.94609893]

    y = [0.02072721, 0.80428563, 0.84872332, 0.86515149, 0.88589646, 0.88686962]
    dey = ['Poggiobello 2013 Sauvignon ',
           'Castello di Buttrio 2014 Sauvignon ',
           'Terredora 2012 Terre Dora ', 'Petrucco 2012 Sauvignon ',
           'Livio Felluga 2010 Pinot Grigio ']

    fig, ax = plt.subplots()

    index = np.arange(len(x))
    bar_width = 0.20

    opacity = 0.65
    error_config = {'ecolor': '0.3'}

    rects1 = ax.bar(index, x, bar_width,
                    alpha=opacity, color='b',
                    error_kw=error_config,
                    label='Conte Leopardi 2012 Calcare Sauvignon (Marche)')

    # rects2 = ax.bar(index + bar_width, y, bar_width,
    #                 alpha=opacity, color='y',
    #                 error_kw=error_config,
    #                 label="Conte Leopardi 2012 Calcare Sauvignon (Marche)")

    ax.set_xlabel('Wines')
    ax.set_ylabel('Scores')
    ax.set_title('Closeness with wine')
    ax.set_xticks(index + bar_width / 2)
    ax.set_xticklabels(('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'))
    ax.legend()
    fig.tight_layout()
    plt.savefig(pathChart + 'plot1' + '.png')
    # categoryfood = ['crudi', 'crostacei', 'salmone', 'formaggio pastamolle', 'formaggio caprino',
    #                 'formaggio stagionato',
    #                 'formaggio mediostagionato', 'formaggio erbolinati', 'pesce magro', 'carni bianche', 'carni rosse',
    #                 'cacciagione', 'funghi e tartufi', 'salumi', 'legumi', 'pasta', 'marinati', 'speziati', 'riso']
    #
    header = ['timestamp', 'user_id', 'wine_id', 'rating', 'index']
    ratings = pd.read_csv(pathCSV + "ratingFromUser.csv", sep=',', names=header, low_memory=False, dtype=int)
    ratings = ratings.drop(['timestamp', 'index'], axis=1)
    from surprise import Reader, Dataset

    x = ratings.shape

    # to load dataset from pandas df, we need `load_fromm_df` method in surprise lib

    ratings_dict = {'user_id': list(ratings.user_id),
                    'wine_id': list(ratings.wine_id),
                    'rating': list(ratings.rating)}
    df = pd.DataFrame(ratings_dict)

    # A reader is still needed but only the rating_scale param is required.
    # The Reader class is used to parse a file containing ratings.
    reader = Reader(rating_scale=(1.0, 5.0))

    # The columns must correspond to user id, item id and ratings (in that order).
    data = Dataset.load_from_df(df[['user_id', 'wine_id', 'rating']], reader)
    data.split(n_folds=10)

    from surprise import SVD, evaluate
    from surprise import NMF

    # svd
    algo = SVD()
    SVD = evaluate(algo, data, measures=['RMSE', 'MAE'])
    print SVD['rmse']
    print SVD['mae']

    cross_validate(algo, data, measures=['RMSE', 'MAE'], cv=10, verbose=True)

    trainset = data.build_full_trainset()
    algo.fit(trainset)
    print algo.predict(14, 10350, 1)

    # nmf
    algo = NMF()
    NMF = evaluate(algo, data, measures=['RMSE', 'MAE'])
    print NMF['rmse']
    print NMF['mae']

    n_groups = 10

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.20

    opacity = 0.65
    error_config = {'ecolor': '0.3'}

    rects1 = ax.bar(index, SVD['rmse'], bar_width,
                    alpha=opacity, color='b',
                    error_kw=error_config,
                    label='SVD rmse')

    rects2 = ax.bar(index + bar_width, SVD['mae'], bar_width,
                    alpha=opacity, color='y',
                    error_kw=error_config,
                    label='SVD mae')

    rects3 = ax.bar(index + 2 * bar_width, NMF['rmse'], bar_width,
                    alpha=opacity, color='r',
                    error_kw=error_config,
                    label='NMF rmse')

    rects4 = ax.bar(index + 3 * bar_width, NMF['mae'], bar_width,
                    alpha=opacity, color='g',
                    error_kw=error_config,
                    label='NMF mae')

    ax.set_xlabel('Group')
    ax.set_ylabel('Scores')
    ax.set_title('RSME between SVD vs NMF ')
    ax.set_xticks(index + bar_width / 2)
    ax.set_xticklabels(('Fold1', 'fold3', 'Fold3', 'Fold4', 'Fold5', 'Fold6', 'fold7', 'Fold8', 'Fold9', 'Fold10'))
    ax.legend()
    ax.set_yscale('log')
    fig.autofmt_xdate()
    fig.tight_layout()
    plt.yscale('log', basey=2)
    plt.savefig(pathChart + 'RSME CollaborativeFilteringMoreData' + str(x) + '.png')
