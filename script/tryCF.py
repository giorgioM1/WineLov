import pandas as pd
import numpy as np
import math

from App.CollaborativeFilter import pathCSV

if __name__ == '__main__':
    header = ['timestamp', 'user_id', 'wine_id', 'rating', 'index']
    rating = pd.read_csv(pathCSV + "ratingFromUser.csv", sep=',', names=header, low_memory=False,
                         dtype=int).sort_values(['timestamp'])
    rating = rating.drop(['timestamp', 'index'], axis=1)
    rating = rating.drop_duplicates(['user_id', 'wine_id'], keep='first')

    mean = rating.groupby(['user_id'], as_index=False, sort=False).mean().rename(
        columns={'rating': 'rating_mean'}).drop(['wine_id'], axis=1)[['user_id', 'rating_mean']]
    # [['userId', 'rating_mean']]
    rating = pd.merge(rating, mean, on='user_id', how='left', sort=False)
    rating['rating_adjusted'] = rating['rating'] - rating['rating_mean']

    distinct_users = np.unique(rating['user_id'])
    user_data_append = pd.DataFrame()
    user_data_all = pd.DataFrame()

    user1_data = rating[rating['user_id'] == 1]
    user1_mean = user1_data['rating'].mean()
    user1_data = user1_data.rename(columns={'rating_adjusted': 'rating_adjusted1'})
    user1_data = user1_data.rename(columns={'user_id': 'user_id1'})
    user1_val = np.sqrt(np.sum(np.square(user1_data['rating_adjusted1']), axis=0))

    distinc_wine = np.unique(rating['wine_id'])

    i = 1
    for wine in distinc_wine[:]:
        item_user = rating[rating['wine_id'] == wine]
        distinct_users1 = np.unique(item_user['user_id'])

        j = 1

        for user2 in distinct_users1:

            #print str(user2) + " user"

            user2_data = rating[rating['user_id'] == user2]

            user2_data = user2_data.rename(columns={'rating_adjusted': 'rating_adjusted2'})
            user2_data = user2_data.rename(columns={'user_id': 'user_id2'})
            user2_val = np.sqrt(np.sum(np.square(user2_data['rating_adjusted2']), axis=0))
            user_data = pd.merge(user1_data, user2_data[['rating_adjusted2', 'wine_id', 'user_id2']], on='wine_id',
                                 how='inner', sort=False)

            user_data['vector_product'] = (user_data['rating_adjusted1'] * user_data['rating_adjusted2'])
            user_data = user_data.groupby(['user_id1', 'user_id2'], as_index=False, sort=False).sum()

            user_data['dot'] = user_data['vector_product'] / (user1_val * user2_val)
            user_data_all = user_data_all.append(user_data, ignore_index=True)
            j = j + 1

        user_data_all = user_data_all[user_data_all['dot'] < 1]
        user_data_all = user_data_all.sort_values(['dot'], ascending=False)
        user_data_all = user_data_all.head(30)
        user_data_all['wine_id'] = wine
        user_data_append = user_data_append.append(user_data_all, ignore_index=True)
        i = i + 1

    User_dot_adj_rating_all = pd.DataFrame()
    j = 1
    for wine in distinc_wine:

        user_data_append_movie = user_data_append[user_data_append['wine_id'] == wine]
        User_dot_adj_rating = pd.merge(rating, user_data_append_movie[['dot', 'user_id2', 'user_id1']], how='inner', left_on='user_id', right_on='user_id2', sort=False)

        # if j % 200 == 0:
        #     print j, "out of", len(distinc_wine)
        User_dot_adj_rating1 = User_dot_adj_rating[User_dot_adj_rating['wine_id'] == wine]

        if len(np.unique(User_dot_adj_rating1['user_id'])) >= 2:
            User_dot_adj_rating1['weighted_rating'] = User_dot_adj_rating1['dot'] * User_dot_adj_rating1['rating_adjusted']

            User_dot_adj_rating1['dot_abs'] = User_dot_adj_rating1['dot'].abs()
            User_dot_adj_rating1 = User_dot_adj_rating1.groupby(['user_id1'], as_index=False, sort=False).sum()[
                ['user_id1', 'weighted_rating', 'dot_abs']]
            User_dot_adj_rating1['rating'] = (User_dot_adj_rating1[
                                                  'weighted_rating'] / User_dot_adj_rating1['dot_abs']) + user1_mean
            User_dot_adj_rating1['wine_id'] = wine
            User_dot_adj_rating1 = User_dot_adj_rating1.drop(['weighted_rating', 'dot_abs'], axis=1)

        User_dot_adj_rating_all = User_dot_adj_rating_all.append(User_dot_adj_rating1, ignore_index=True)

        j = j + 1

    User_dot_adj_rating_all = User_dot_adj_rating_all.sort_values(['rating'], ascending=False)
    print User_dot_adj_rating_all
