import pandas as pd

from App.CollaborativeFilter import pathCSV

pathModel = "/home/giorgio/Scrivania/WineLov/Question/"


def addCategory(wines):
    food_matching = []
    id = []
    for i in range(0, len(wines)):
        id.append(wines.iloc[i].id_wine)
        # print wines.iloc[i].variety
        if (wines.iloc[i].variety.lower().find('barbera')) is not -1:
            food_matching.append('formaggio stagionato, formaggio erbolinati, carni rosse, funghi e tartufi')
        elif wines.iloc[i].variety.lower().find('aglianico') is not -1:
            food_matching.append('carni bianche, carni rosse, cacciagione, salumi')
        elif wines.iloc[i].variety.lower().find('cabernet franc') is not -1:
            food_matching.append('funghi e tartufi')
        elif wines.iloc[i].variety.lower().find('arneis') is not -1:
            food_matching.append('formaggio pastamolle, pesce magro, crudi')
        elif wines.iloc[i].variety.lower().find('cabernet sauvignon') is not -1:
            food_matching.append('formaggio stagionato, legumi, marinati, speziati')
        elif wines.iloc[i].variety.lower().find('cannonau') is not -1:
            food_matching.append('cacciagione, formaggio stagionati, cacciagione, carni rosse')
        elif wines.iloc[i].variety.lower().find('chardonnay') is not -1:
            food_matching.append('formaggio pastamolle, formaggio caprino, pesce magro, crostacei, carni bianche')
        elif wines.iloc[i].variety.lower().find('dolcetto') is not -1:
            food_matching.append(
                'formaggio caprino, formaggio stagionato, cacciagione, funghi e tartufi, legumi, pasta')
        elif wines.iloc[i].variety.lower().find('falanghina') is not -1:
            food_matching.append('pesce magro, carni bianche, salumi')
        elif wines.iloc[i].variety.lower().find('gew') is not -1:
            food_matching.append('crudi, crostacei, salmone, marinati, speziati')
        elif wines.iloc[i].variety.lower().find('greco') is not -1:
            food_matching.append('crudi, crostacei, salmone, pesce magro, pasta, formaggio pastamolle')
        elif wines.iloc[i].variety.lower().find('insolia') is not -1:
            food_matching.append('funghi e tartufi')
        elif wines.iloc[i].variety.lower().find('lagrein') is not -1:
            food_matching.append('crudi, crostacei, salmone')
        elif wines.iloc[i].variety.lower().find('merlot') is not -1:
            food_matching.append('formaggio mediostagionato, carni bianche, carni rosse, marinati, speziati, riso')
        elif wines.iloc[i].variety.lower().find('montepulciano') is not -1:
            food_matching.append('formaggio erbolinati, carni bianche, cacciagione, carni rosse, salumi, riso')
        elif wines.iloc[i].variety.lower().find('thurgau') is not -1:
            food_matching.append('crudi, crostacei, salmone, marinati, speziati')
        elif wines.iloc[i].variety.lower().find('nebbiolo') is not -1:
            food_matching.append('formaggio stagionato, carni rosse, carni bianche, cacciagione, salumi')
        elif wines.iloc[i].variety.lower().find("nero d'avola") is not -1:
            food_matching.append('carni bianche, salumi, marinati, speziati')
        elif wines.iloc[i].variety.lower().find('pecorino') is not -1:
            food_matching.append('formaggio caprino, formaggio mediostagionato')
        elif wines.iloc[i].variety.lower().find('pinot bianco') is not -1:
            food_matching.append('formaggio pastamolle, salumi, pesce magro, pasta')
        elif wines.iloc[i].variety.lower().find('pinot grigio') is not -1:
            food_matching.append('carni bianche, pesce magro, riso')
        elif wines.iloc[i].variety.lower().find('pinot nero') is not -1:
            food_matching.append('formaggio mediostagionato, funghi e tartufi, cacciagione, formaggio stagionato, carni bianche')
        elif wines.iloc[i].variety.lower().find('primitivo') is not -1:
            food_matching.append('carni rosse, riso, cacciagione, formaggio stagionato')
        elif wines.iloc[i].variety.lower().find('ribolla gialla') is not -1:
            food_matching.append('salumi, marinati, speziati, crudi, pesce magro')
        elif wines.iloc[i].variety.lower().find('riesling') is not -1:
            food_matching.append('riso')
        elif wines.iloc[i].variety.lower().find('sagrantino') is not -1:
            food_matching.append('formaggio erbolinati, carni rosse, salumi')
        elif wines.iloc[i].variety.lower().find('sangiovese') is not -1:
            food_matching.append('formaggio caprino, carni rosse, salumi')
        elif wines.iloc[i].variety.lower().find('sauvignon') is not -1:
            food_matching.append('formaggio caprino, formaggio mediostagionato, crostacei, legumi, marinati, speziati')
        elif wines.iloc[i].variety.lower().find('verdicchio') is not -1:
            food_matching.append('formaggio caprino, funghi e tartufi, legumi')
        elif wines.iloc[i].variety.lower().find('vermentino') is not -1:
            food_matching.append('pesce magro, pasta')
        elif wines.iloc[i].variety.lower().find('vernaccia') is not -1:
            food_matching.append('salumi, pesce magro, riso, pasta, marinati, crudi, carni bianche')
        else:
            food_matching.append('')

    return id, food_matching


def main():
    header = ['id_wine', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']
    df = pd.read_csv(pathCSV + 'DB.csv', sep=',', header=None, names=header)
    col = ['id_wine', 'name', 'description', 'variety']

    wines = df[col]
    id, food_matching = addCategory(wines)
    print len(food_matching)

    matching = pd.DataFrame({'id_wine': id, 'food': food_matching})
    header = ['id_wine', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']
    col = ['id_wine', 'name', 'description', 'variety']
    df = pd.read_csv(pathCSV + 'DB.csv', sep=',', names=header, low_memory=False)
    df = df[col]
    match = pd.merge(df, matching)
    print match.shape
    match.to_csv(pathCSV + 'matchingFood.csv', sep=',', header=None, index=False)


if __name__ == '__main__':
    main()
