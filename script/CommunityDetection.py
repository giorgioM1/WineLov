import csv

import igraph
from igraph import *

import pandas as pd

from App.CollaborativeFilter import pathCSV, pathDB

if __name__ == '__main__':
    header = ['timestamp', 'user_id', 'wine_id', 'rating', 'index']
    ratings = pd.read_csv(pathCSV + "ratingFromUser.csv", sep=',', names=header, low_memory=False, dtype=int)
    ratings = ratings.drop(['timestamp', 'index'], axis=1)

    x = ratings[ratings.wine_id == 6003]
    uniqueW = ratings.wine_id.unique()
    k = 0
    for i in uniqueW:
        print "wine  id: " + str(i)
        ratingForWine = ratings[ratings.wine_id == (i)]
        uniqueUserForWine = ratingForWine.user_id.unique()
        if len(uniqueUserForWine) != 1:
            k += 1
            for k in range(1, len(uniqueUserForWine)):
                # print uniqueUserForWine[0], uniqueUserForWine[k]
                with open(pathCSV + 'community.csv', 'aw') as csvfile:
                    writer = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                    writer.writerow([uniqueUserForWine[0], uniqueUserForWine[k]])

        # firstUser = ratingForWine.user_id.iloc[0]

    header = ['user1', 'user2']
    community = pd.read_csv(pathCSV + "community.csv", sep=',', names=header, low_memory=False, dtype=int)
    print community.shape
    community = community.drop_duplicates()
    print community
    print community.shape
    user1 = community.user1
    user2 = community.user2
    print user1
    print user2
    print community
    G = Graph()
    for k in range(0, 1000):
    # for row in community.iterrows():
    #     print row
        G.add_vertices(user1.iloc[k])
        G.add_vertices(user2.iloc[k])
        G.add_edges([(user1.iloc[k], user2.iloc[k])])

    print G.get_edgelist()[0:10]
