import urllib2
import requests
import time
from lxml import html
import json
import redis
from bs4 import BeautifulSoup
from user_agent import generate_user_agent, generate_navigator


def getPage(urls):
    headers = requests.utils.default_headers()
    #headers.update({'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',})
    h = generate_user_agent()
    headers.update({'User-Agent' : str(h),})
    page = requests.get(urls,headers=headers)
    return page


def createItem(vectorValue):
    item = ""
    for value in vectorValue:
        item = value + ","
    item = item[:-1]
    return item


def getDataFromLink(url,db,index):
    page = ''
    while True:
        try:
            page = getPage(url)
            jsondata = {}
            #now we get the values
            soup = BeautifulSoup(page.content,'html.parser')
            content = []
            content =  soup.find_all("script", type="application/ld+json")

            name = soup.find("div", class_="article-title").find(text=True)
            jsondata['name'] = name

            point = soup.find("span", id="points").find(text=True)
            jsondata['point'] = point

            description = soup.find("p", class_="description").find(text=True)
            jsondata['description'] = description

            primaryinfo = soup.find("ul", class_="primary-info")
            needsPrimary = []
            for li_tag in soup.find_all('ul', {'class':'primary-info'}):
             for span_tag in li_tag.find_all('li', {'class':'row'}):
                 field = span_tag.find('span').text
                 value = span_tag.find("div", class_="info medium-9 columns").text
                 needsPrimary.append(value)

            needsPrimary[0] = needsPrimary[0].split(',')[0].strip(' \t\n\r')
            price = needsPrimary[0]
            price = price.replace("$"," ")
            jsondata['price'] = price

            if len(needsPrimary) == 4:
                needsPrimary[1] = needsPrimary[1].replace(',',"").strip(' \t\n\r')
                variety = needsPrimary[1]
                jsondata['variety'] = variety

                needsPrimary[2] = needsPrimary[2].replace(',', "").strip(' \t\n\r')
                location = needsPrimary[2]
                jsondata['location'] = location

                needsPrimary[3] = needsPrimary[3].strip(' \t\n\r')
                winery = needsPrimary[3]
                jsondata['winery'] = winery

            else:
                needsPrimary[2] = needsPrimary[2].replace(',',"").strip(' \t\n\r')
                variety = needsPrimary[2]
                jsondata['variety'] = variety

                needsPrimary[3] = needsPrimary[3].replace(',', "").strip(' \t\n\r')
                location = needsPrimary[3]
                jsondata['location'] = location

                needsPrimary[4] = needsPrimary[4].strip(' \t\n\r')
                winery = needsPrimary[4]
                jsondata['winery'] = winery

            needsSecondary = []
            for li_tag in soup.find_all('ul', {'class':'secondary-info'}):
             for span_tag in li_tag.find_all('li', {'class':'row'}):
                 field = span_tag.find('span').text
                 value = span_tag.find("div", class_="info small-9 columns").text
                 needsSecondary.append(value)
            needsSecondary = needsSecondary[0:3]

            alchol = needsSecondary[0].strip(' \t\n\r')
            jsondata['alchol'] = alchol

            dimension = needsSecondary[1].strip(' \t\n\r')
            jsondata['dimension'] = dimension

            type = needsSecondary[2].strip(' \t\n\r')
            jsondata['type'] = type
            break
        except:
            print "error to retrival page"
            time.sleep(2)
            continue

    jsonValue = json.dumps(jsondata)
    #item = createItem(feature)
    x = db.set(index,jsonValue)
    if(x!=True):
        print "error in DB"


def getLinkListPage(page):
    soup = BeautifulSoup(page.content,'html.parser')
    link = soup.find_all('a', class_="review-listing")
    return link
    # for i in link:
    #     print i.get("href")
    #     getDataFromLink(i.get("href"))
    #     time.sleep(1)


def createDB():
    r = redis.Redis(
        host="localhost",
        port=6379,
        db = 0)
    # r.flushall()
    # r.flushdb()
    return r


def main():
    db = createDB()
    j = 1;
    index = 0
    while True:
        winemag1 = "https://www.winemag.com/?s=&drink_type=wine&wine_type=Red,White&country=Italy&page="+str(j);
        while True:
            try:
                page = getPage(winemag1)
                break
            except:
                print "Error get page"
                time.sleep(2)
                continue

        if(page.status_code != 200):
            break
        linkListWine = []
        linkListWine = getLinkListPage(page)
        for i in linkListWine:
            #print i.get("href")
            #try:
            getDataFromLink(i.get("href"),db,index)
            index = index + 1
            # except:
            #     print("Connection refused by the server..")
            #     print("Let me sleep for 5 seconds")
            #     time.sleep(5)
            #     print("Was a nice sleep, now let me continue...")
            #     continue

        # print "----------------------------------------------------------------------------------------"
        # print "page " + str(j) +" number saved Wine " + str(index)
        # print "----------------------------------------------------------------------------------------"

        j = int(j)
        j = j+1;



if __name__ == '__main__':
    main()
