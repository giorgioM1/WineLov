import json
from kafka import KafkaProducer
from kafka.errors import KafkaError


class ProducerStatistics:
    def __init__(self, topic, host, port):
        self.host = host
        self.port = port
        self.topic = topic
        self.producer = None
        self.initProducer()

    def initProducer(self):
        hosting = self.host + ':' + str(self.port)
        self.producer = KafkaProducer(bootstrap_servers=[hosting],
                                      value_serializer=lambda m: json.dumps(m).encode('utf-8'))

    def send(self, data):
        self.producer.send(self.topic, data)


if __name__ == '__main__':

    prd = ProducerStatistics('my-topic', 'localhost', 9092)
    for _ in range(100):
        prd.send({'key': 'valuessssss', 'name': 'giorgio', 'surname': 'marini'})

    # producer = KafkaProducer(bootstrap_servers=['localhost:9092'])

    # # Asynchronous by default
    # future = producer.send('my-topic', b'raw_bytes')
    #
    # # Block for 'synchronous' sends
    # try:
    #     record_metadata = future.get(timeout=10)
    # except KafkaError:
    #     # Decide what to do if produce request failed...
    #     log.exception()
    #     pass
    #
    # # Successful result returns assigned partition and offset
    # print (record_metadata.topic)
    # print (record_metadata.partition)
    # print (record_metadata.offset)
    #
    # # produce keyed messages to enable hashed partitioning
    # producer.send('my-topic', key=b'foo', value=b'bar')
    #
    # # encode objects via msgpack
    # producer = KafkaProducer(value_serializer=msgpack.dumps)
    # producer.send('msgpack-topic', {'key': 'value'})
    #
    # # produce json messages
    # producer = KafkaProducer(bootstrap_servers=['localhost:9092'], value_serializer=lambda m: json.dumps(m).encode('utf-8'))
    # producer.send('json-topic', {'key': 'value'})

    # produce asynchronously

    #
    # def on_send_success(record_metadata):
    #     print(record_metadata.topic)
    #     print(record_metadata.partition)
    #     print(record_metadata.offset)
    #
    #
    # def on_send_error(excp):
    #     log.error('I am an errback', exc_info=excp)
    #     # handle exception
    #
    #     # block until all async messages are sent
    #     producer.flush()
    #
    #     # configure multiple retries
    #     producer = KafkaProducer(retries=5)

    # produce asynchronously with callbacks
    # producer.send('my-topic', b'raw_bytes').add_callback(on_send_success).add_errback(on_send_error)
