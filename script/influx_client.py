from influxdb import InfluxDBClient


class InfluxConnection:
    def __init__(self, user='root', password='root', host='localhost', port=8086, name='ratings'):
        self.host = host
        self.password = password
        self.user = user
        self.dbname = name
        self.port = port
        self.client = None
        self.getConnection()
        # self.dropDb()
        # self.createDatabase()

    def getConnection(self):
        conn = InfluxDBClient(self.host, self.port, self.user, self.password, database=self.dbname)
        self.client = conn

    def endPointConnection(self):
        return self.client

    def createDatabase(self):
        self.client.create_database(self.dbname)

    def dropDb(self):
        self.client.drop_database(self.dbname)

