#!/usr/bin/env python
# -*- coding: utf-8 -*-
import redis
import json
import io
import string
import re
import nltk
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk import sent_tokenize
from nltk.tokenize import word_tokenize
from collections import Counter
from operator import itemgetter





def main():
    nltk.download('stopwords')
    stop_words = stopwords.words('english')
    #print(stop_words)

    r = redis.Redis(
        host="localhost",
        port=6379,
        db = 0)

    #raw = open("raw.json","w+")

    raw  = io.open("raw.txt", mode="w+", encoding="utf-8")
    k  = 0
    labels = []
    for i in range(0,27000):
        data = json.loads(r.get(i))
        des = data["description"]
        labels.append(data["variety"])
        if des != None:
            for p in string.punctuation:
                des = des.replace(p,' ')
                des = des.lower()
        if data["type"] == "White":
            k +=1
            if k == 5000:
                break
            else:
                raw.write(des)
        elif data["type"] != "Red":
            print data["type"]
    raw.close()
    print " how many white are there in the cellare? there are : " + str(k)
    t = open("raw.txt")

    rawInMem = t.read()
    x = rawInMem.split()
    print len(x)

    # split into words
    tokens = word_tokenize(rawInMem)
    # convert to lower case
    tokens = [w.lower() for w in tokens]
    # filter out stop words
    stop_words = set(stopwords.words('english'))
    words = [w for w in tokens if not w in stop_words]
    #print(words[:])
    #
    # print string.punctuation
    # for p in string.punctuation:
    #     rawInMem = rawInMem.replace(p,' ')
    # print rawInMem
    # words = rawInMem.split()
    # print(words[:])
    #
    # sentences = sent_tokenize(rawInMem)
    # print sentences


    occorenses = open("occorensesWhiteWine.json", mode="w+")
    wordcount = Counter(words)
    for it in list(wordcount):
        if wordcount[it] < 15:
             del wordcount[it]
    print len(wordcount)

    v = list(wordcount.values())
    x = list(wordcount)
    wordcountjson = [{"key": t, "Value": s} for t, s in zip(x, v)]
    rows_by_value = sorted(wordcountjson, key=itemgetter('Value'), reverse=True)
    jsonValue = json.dumps(rows_by_value)
    occorenses.write(jsonValue)
    occorenses.close()
    print wordcount

    labelscount = Counter(labels)
    for it in list(labelscount):
        if labelscount[it] < 10:
             del labelscount[it]
    print labelscount

if __name__ == '__main__':

  main()
