#!/usr/bin/env python
# -*- coding: utf-8 -*-
import redis
import json
import io
import string
import re
import csv
import sys

reload(sys)
sys.setdefaultencoding('utf8')

pathDB = "/home/giorgio/Scrivania/WineLov/DB/"
pathCSV = "/home/giorgio/Scrivania/WineLov/CSV/"


def createFileDB():
    r = redis.Redis(
        host="localhost",
        port=6379,
        db=0)

    raw = open(pathDB + "DB.json", mode="w+")
    dbsize = r.dbsize()
    DB = []
    for i in range(0, dbsize):
        data = json.loads(r.get(i))
        if data == None:
            break
        data['id'] = i + 1
        DB.append(data)
    raw.write(json.dumps(DB))
    raw.close()


def createFileDBType(type):
    r = redis.Redis(
        host="localhost",
        port=6379,
        db=0)

    filename = "DB" + str(type) + ".json"
    raw = open(pathDB + filename, mode="w+")
    dbsize = r.dbsize()
    DB = []
    i = 1
    for i in range(0, dbsize):
        data = json.loads(r.get(i))
        if data == None:
            break
        elif data["type"] == type:
            data["id"] = i + 1
            DB.append(data)
    raw.write(json.dumps(DB))
    raw.close()


def getWineForSurvey(DB):
    valuesForType = 100
    raw = json.loads(DB)
    countW = 0
    countR = 0
    foundWhite = False
    foundRed = False
    survey = []
    white = []
    for i in range(0, len(raw)):
        if foundWhite and foundRed:
            break
        if raw[i]["type"] == "Red":
            if foundRed == True:
                continue
            survey.append(raw[i])
            countR += 1
            if countR == valuesForType:
                foundRed = True
        if raw[i]["type"] == "White":
            if foundWhite == True:
                continue
            survey.append(raw[i])
            white.append(raw[i])
            countW += 1
            if countW == valuesForType:
                foundWhite = True

    with open(pathDB + 'survey.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for value in survey:
            writer.writerow([value["id"], value["name"], value["variety"]])


def createCSVvariety(DB):
    listWinery = []
    for row in DB:
        listWinery.append(row['variety'])
    setCat = set(listWinery)
    for v in set(listWinery):
        print v

    with open(pathCSV + 'category.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        i = 1
        for value in setCat:
            writer.writerow([i, value])
            i += 1


def createCSVWine(data):
    with open(pathCSV + 'wineCF.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for row in data:
            if row['description'] is not None:
                writer.writerow([row['id'], row['name'], row['variety'], row['id']])


""" script to create many file about CSV or Json to produce output
"""


def main():
    # createFileDB()
    # createFileDBType("White")
    # createFileDBType("Red")
    DB = open(pathDB + "DB.json", mode="r").read()
    data = json.loads(DB)
    # createCSVvariety(data)

    createCSVWine(data)

    # getWineForSurvey(DB)


if __name__ == '__main__':
    main()
