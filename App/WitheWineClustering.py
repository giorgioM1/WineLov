import collections
import json
import re

import nltk
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import word_tokenize
from sklearn.cluster import KMeans
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer

path = "/home/giorgio/Scrivania/WineLov/DB/"
pathModel = "/home/giorgio/Scrivania/WineLov/ModelK/"


def getWine(fileJDB):
    wine = []
    # for i in range(0,len(fileJDB)):
    # print fileJDB[i]["name"]
    for i in range(0, len(fileJDB)):
        wine.append(fileJDB[i]["name"])
    return wine


def getDescription(fileJDB):
    description = []
    #    for i in range(0,len(fileJDB)):
    for i in range(0, len(fileJDB)):
        description.append(fileJDB[i]["description"])
    return description


def getVariety(fileJDB):
    variety = []
    for i in range(0, len(fileJDB)):
        variety.append(fileJDB[i]["variety"])
    return variety


def clean(fileJDB):
    i = 0
    while True:
        try:
            if fileJDB[i]["description"] is None:
                del fileJDB[i]
            else:
                i += 1
        except:
            print "finish"
            break
    return fileJDB


def tokenize_and_stem_and_stopword(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    stems = []
    stemmer = SnowballStemmer("english")
    tokens = word_tokenize(text)
    # lower case
    tokens = [w.lower() for w in tokens]
    # remove stop words
    stop_words = set(stopwords.words('english'))
    words = [w for w in tokens if not w in stop_words]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in words:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)

    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    stop_words = set(stopwords.words('english'))
    words = [w for w in tokens if not w in stop_words]
    filtered_tokens = []

    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in words:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens


def showCluster(frame, vocab_frame, km, num_clusters, terms):
    values = frame['clusters'].value_counts()
    print values

    print("Top terms per cluster:")
    # sort cluster centers by proximity to centroid
    order_centroids = km.cluster_centers_.argsort()[:, ::-1]

    for i in range(num_clusters):
        print "Cluster %s words:" % i

        for ind in order_centroids[i, :10]:  # replace 6 with n words per cluster
            print ' %s' % vocab_frame.ix[terms[ind].split(' ')].values.tolist()[0][0].encode('utf-8', 'ignore')

        print "Cluster %d wines:" % i
        j = 0
        for name in frame.ix[i]['wines'].values.tolist():
            print name
            j += 1
            if j == 3:
                break
        print "Cluster %d description:" % i
        j = 0
        for description in frame.ix[i]['description'].values.tolist():
            print description
            j += 1
            if j == 3:
                break

        print "Cluster %d variety:" % i
        lists = []
        for variety in frame.ix[i]['variety'].values.tolist():
            lists.append(variety)
        c = collections.Counter(lists)
        print c


def main():
    x = open(path + "DBWhite.json", mode="r").read()
    jDB = json.loads(x)
    newjDB = clean(jDB)
    print "exit"
    wine = getWine(newjDB)
    description = getDescription(newjDB)
    variety = getVariety(newjDB)
    #print collections.Counter(variety)
    #print len(description)

    VoctokenizeDescription = []
    VoctokenizeDescriptionStemmed = []
    raw = []

    for value in description:
        stemmedword = tokenize_and_stem_and_stopword(value)
        VoctokenizeDescriptionStemmed.extend(stemmedword)
        tokenizeWord = tokenize_only(value)
        for v in tokenizeWord:
            raw.append(v)
        VoctokenizeDescription.extend(tokenizeWord)

    word = collections.Counter(raw)
    for v in word:
        if word[v] > 100:
            print v, word[v]



    vocab_frame = pd.DataFrame({'words': VoctokenizeDescription}, index=VoctokenizeDescriptionStemmed)

    # vectorized parameters
    tfidf_vectorizer = TfidfVectorizer(max_df=0.30, max_features=300000,
                                       min_df=0.03, stop_words='english',
                                       use_idf=True, tokenizer=tokenize_and_stem_and_stopword, ngram_range=(1, 2))
    # fit the vectors to description
    tfidf_matrix = tfidf_vectorizer.fit_transform(description)
    # print tfidf_matrix
    # print tfidf_matrix.shape
    terms = tfidf_vectorizer.get_feature_names()

    num_clusters = 5

    km = KMeans(n_clusters=num_clusters)

    km.fit(tfidf_matrix)

    clusters = km.labels_.tolist()
    # if i'd like to export the training model
    joblib.dump(km, pathModel + "model_clusterWhiteWine.pkl")
    #
    # km = joblib.load('doc_cluster.pkl')
    # clusters = km.labels_.tolist()
    print clusters

    wines = {'wines': wine, 'description': description, 'clusters': clusters, 'variety': variety}
    frame = pd.DataFrame(wines, index=[clusters], columns=['wines', 'description', 'variety', 'clusters'])

    showCluster(frame, vocab_frame, km, num_clusters, terms)

    x = []
    print "new value"
    print jDB[1500]["description"]
    x.append(jDB[1500]["description"])
    # convert a description to vector to confront with other clusters
    Nd = tfidf_vectorizer.transform(x)
    # print terms
    print Nd
    print "prediction" + str(km.predict(Nd))

    #
    # if fileJDB[i]["type"] == "White":
    #     white +=1
    # elif fileJDB[i]["type"] == "Red":
    #     red +=1


if __name__ == '__main__':
    main()
