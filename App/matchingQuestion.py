import csv
import json
import sys
import pandas as pd
from sklearn import cross_validation as cv

from similarityDesc import similarity

reload(sys)
sys.setdefaultencoding('utf8')

pathCSV = "/home/giorgio/Scrivania/WineLov/CSV/"
pathQuestion = "/home/giorgio/Scrivania/WineLov/Question/"


def createKeyWord(givenanswer):
    x = open(pathQuestion + "Question.json", mode="r").read()
    jDB = json.loads(x)
    keyword = []
    i = 1
    for e in jDB:
        # print givenanswer[i]
        if givenanswer[i] is 'A':
            x = e['A'].split(',')
            keyword.extend(x)
        elif givenanswer[i] is 'B':
            x = e['B'].split(',')
            keyword.extend(x)
        i += 1

    return set(keyword)


def adaptResponse():
    with open(pathCSV + "WineLoversContent.csv", 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_NONE)
        i = 0
        for row in reader:
            first = row
            i += 1
            break
        i = 0
        rows = []
        for row in reader:
            row
            i += 1
            if i >= 1:
                rows.append(row)
        return first, rows


class matchingAnswer():
    def __init__(self):
        self.question = None

    def parseAnswer(self, answer):
        print answer
        user = answer[1].replace('"', '').strip()
        givenanswer = []
        givenanswer.append(user)
        for v in answer[2:]:
            # print v
            tmp = v.split('-')
            tmp[0] = tmp[0].replace('"', '').strip()
            givenanswer.append(tmp[0])

        print givenanswer
        set = createKeyWord(givenanswer)
        #print set
        key = ""
        for s in set:
            key += (s.strip() + ' ')
        print(key)
        return key

    def mappingQuestion(self, question):
        id_q = []
        i = 0
        for v in question[1:]:
            tmp = v.split("-")
            t = tmp[0].replace('"', '').strip()
            id_q.append(t)
        self.question = id_q
        return id_q

    def parseAnswerSimple(self,listAns):
        #print listAns
        set = createKeyWord(listAns)
        # print set
        key = ""
        for s in set:
            key += (s.strip() + ' ')
        #print(key)
        return key
