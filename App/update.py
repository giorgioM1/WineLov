from threading import Event, Thread

import pandas as pd

from CollaborativeFilter import collaborativeFiltering
from GraphCollaborativeFiltering import pathCSV

header = ['timestamp', 'user_id', 'wine_id', 'rating', 'index']
sparse = pd.read_csv(pathCSV + "ratingFromUser.csv", sep=',', names=header, low_memory=False, dtype=int).sort_values(
    ['timestamp'])
# sparse = sparse.drop(['timestamp'], axis=1)
dfsparse = sparse.drop_duplicates(['user_id', 'wine_id'], keep='first')


class UpdateModel:
    def __init__(self, ready=None):
        self.ready = ready
        self.CF = None

    def make(self):
        header = ['timestamp', 'user_id', 'wine_id', 'rating', 'index']
        sparse = pd.read_csv(pathCSV + "ratingFromUser.csv", sep=',', names=header, low_memory=False,dtype=int).sort_values(['timestamp'])
        # sparse = sparse.drop(['timestamp'], axis=1)
        dfsparse = sparse.drop_duplicates(['user_id', 'wine_id'], keep='first')
        # lets update the model
        self.CF = collaborativeFiltering(dfsparse, n=20)

        # then fire the ready event
        self.ready.set()

    def newModel(self):
        # do something with self.connection
        return self.CF


if __name__ == '__main__':
    ready = Event()
    program = UpdateModel(ready)

    # configure & start thread
    thread = Thread(target=program.make)
    thread.start()
    ready.wait()
    print "do smething"
    print "do smething"
    print "do smething"
    print "do smething"
    print "do smething"
    print "do smething"
    print "do smething"
    print "do smething"

    # block until ready

    # now we can safely use program
    x = program.newModel()
