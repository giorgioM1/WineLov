import pandas as pd
import numpy as np
# import matplotlib as plt
from py2neo import Graph
from py2neo.packages.httpstream import http

http.socket_timeout = 9999

# configuration path for neo4j DB
graph = Graph('http://neo4j:admin@localhost:7474/db/data/')
pathCSV = "/home/giorgio/Scrivania/WineLov/CSV/"


def proportion(point):
    return (5 * float(point)) / float(100)


class GraphCollaborativeFilter:
    def __init__(self):
        self.g = graph

    def createBasedWine(self, wines, genre):
        tx = self.g.cypher.begin()
        statement1 = "MERGE (a:`Wine`{wine_id:{A}, name:{B}, description:{C}, variety:{D}, point:{E}}) RETURN a"
        statement2 = ("MATCH (t:`category`{cat_id:{G}}) "
                      "MATCH (a:`Wine`{wine_id:{A}}) MERGE (a)-[r:`Is_variety`]->(t) RETURN r")

        # ss = {"A": w.iloc[0], "B": w.iloc[1], "C": w.iloc[2], "D": w.iloc[3],"E": w.iloc[4]}
        for m, w in wines.iterrows():
            tx.append(statement1,
                      {"A": w.iloc[0], "B": w.iloc[1], "C": w.iloc[2], "D": w.iloc[3], "E": w.iloc[4]})

            for g in genre.iterrows():
                if g[1]['category'] == w.iloc[3]:
                    # print g[1]['category'], w.iloc[3]
                    tx.append(statement2,
                              {"A": w.iloc[0], "G": g[1]['id']})

            if m % 100 == 0:
                tx.process()
                print "save"
        tx.commit()

    def UpdateWineBase(self, wines):
        tx = self.g.cypher.begin()
        statement1 = "MERGE (a:`Wine`{wine_id:{A}})  SET a.rating = {rating} RETURN a"

        for m, w in wines.iterrows():
            rating = proportion(w.iloc[4])
            # print rating
            tx.append(statement1,
                      {"A": w.iloc[0], "rating": rating})
            if m % 100 == 0:
                tx.process()
                # print "save"

        tx.commit()

    def createBasedCategories(self, genre):
        # n_g = genre.shape

        tx = self.g.graph.cypher.begin()
        statement = "MERGE (a:`category`{cat_id:{A}, variety:{B}}) RETURN a"

        for m, g in genre.iterrows():
            tx.append(statement, {"A": g.iloc[0], "B": g.iloc[1]})
            if m % 100 == 0:
                tx.process()
        tx.commit()

    def createBaseUser(self):
        rate = pd.read_csv(pathCSV + 'rated.csv', sep=',', header=None, names=['id_user', 'wine_id', 'rating'])
        print rate.shape
        tx = self.g.cypher.begin()
        statement = "MERGE (a:`User`{user_id:{U}, name:{W}}) RETURN a"
        statementRel = ("MATCH (t:`Wine`{wine_id:{W}}) "
                        "MATCH (a:`User`{user_id:{U}}) MERGE (a)-[r:`has_rated`{rating:{R}}]->(t)                     RETURN r")
        for m, r in rate.iterrows():
            tx.append(statement, {"U": r.iloc[0], "W": "ciccio"})
            tx.append(statementRel, {"U": r.iloc[0], "W": r.iloc[1], "R": r.iloc[2]})
            if m % 100 == 0:
                tx.process()
        tx.commit()

    def addNewUserRating(self, user_id, name, wine_id, score):
        tx = self.g.cypher.begin()
        statement = "MERGE (a:`User`{user_id:{U}, name:{W}}) RETURN a"
        statementRel = ("MATCH (t:`Wine`{wine_id:{W}}) "
                        "MATCH (a:`User`{user_id:{U}}) MERGE (a)-[r:`has_rated`{rating:{R}}]->(t) RETURN r")
        tx.append(statement, {"U": user_id, "W": name})
        tx.append(statementRel, {"U": user_id, "W": wine_id, "R": score})
        tx.commit()
        self.updateRating(wine_id, score)

    def addNewUserNew(self, name, taste):
        tx = self.g.cypher.begin()
        statement = "MATCH (n: User) RETURN count(n) as n"
        tx.append(statement)
        res = tx.commit()
        user = 0
        for r in res:
            if len(r) == 0:
                break
            else:
                v = r[0]['n']
                print v
                user = v
        user += 1
        print user
        tx = self.g.cypher.begin()
        statement = "MERGE (a: User{user_id:{U}, name:{W}, tastes:{T}}) RETURN a"
        tx.append(statement, {"U": user, "W": name, "T": taste})
        return tx.commit()

    def addNewUser(self, name):
        tx = self.g.cypher.begin()
        statement = "MATCH (n: User) RETURN count(n) as n"
        tx.append(statement)
        res = tx.commit()
        user = 0
        for r in res:
            if len(r) == 0:
                break
            else:
                v = r[0]['n']
                print v
                user = v
        user += 1
        print user
        tx = self.g.cypher.begin()
        statement = "MERGE (a: User{user_id:{U}, name:{W}}) RETURN a"
        tx.append(statement, {"U": user, "W": name})
        tx.commit()

    def addNewRatings(self, user_id, wine_id, score):
        self.updateRating(wine_id, score)
        tx = self.g.cypher.begin()
        statement = ("MATCH (u:User{user_id:{U}}) "
                     "MATCH (w:Wine{wine_id:{W}}) "
                     "MERGE (u)-[r:has_rated{rating:{R}}]->(w) "
                     "RETURN r")
        tx.append(statement, {"U": user_id, "W": wine_id, "R": score})
        tx.commit()
        return True

    def getWine(self, wine_id):
        tx = self.g.cypher.begin()
        query = "MATCH (w:Wine{wine_id:{wine_id}}) return w.description as result"
        tx.append(query, {"wine_id": wine_id})
        des = tx.commit()
        if len(des[0]) == 0:
            return []
        for r in des:
            if len(r) == 0:
                break
            else:
                return r[0]['result']

    def findSimilarityJaccard(self, user):

        tx = self.g.cypher.begin()
        query = (  # intersection of the wine from the user
            'MATCH (u1:User{user_id:{user_id}})-[:has_rated]->(w:Wine)<-[:has_rated]-(u2:User) '
            'WHERE u1 <> u2 '
            'WITH u1, u2, COUNT(DISTINCT w) as intersection_count '
            # mathc union of the wine that they rated
            'MATCH (u:User)-[:has_rated]->(w:Wine) '
            'WHERE u in [u1, u2] '
            'WITH u1, u2, intersection_count, COUNT(DISTINCT w) as union_count '
            # calculate jaccard_similarity from user
            'WITH u1, u2, intersection_count, union_count, (intersection_count*1.0/union_count) as jaccard_index '
            'ORDER BY jaccard_index DESC, u2.user_id WITH u1, COLLECT(u2)[0..{k}] as neighbours '
            'WHERE LENGTH(neighbours) = {k} '
            'UNWIND neighbours as neighbour '
            'WITH u1, neighbour '
            'MATCH (neighbour)-[:has_rated]->(w:Wine) '
            'WHERE not (u1)-[:has_rated]->(w:Wine) and w.rating >= {thresholdRating} '
            'WITH u1, w, COUNT(DISTINCT neighbour) as cnt '
            'ORDER BY u1.user_id, cnt DESC '
            'RETURN u1.user_id as user,COllECT(w.wine_id)[0..{n}] as wine_id, COLLECT(w.name)[0..{n}] as recos, COLLECT(w.rating) as rat, COLLECT(w.description) as descriptions')

        # k indicates the users with more than K neighbours, while n indicates the number of wine to return
        tx.append(query, {'user_id': user, 'k': 5, 'n': 10, 'thresholdRating': 3})
        res = tx.commit()
        print res
        wines = []
        id_w = []
        description = []
        result = []
        if len(res[0]) == 0:
            return wines, id_w, description
        for r in res:
            if len(r) == 0:
                break
            else:
                resultD = {}
                for v in r[0]['recos']:
                    wines.append(v)
                    resultD['wine'] = v
                for i in r[0]['wine_id']:
                    id_w.append(i)
                    resultD['id'] = i
                print resultD
                result.append(resultD)
                # for i in r[0]['rat']:
                # this is the proof of function to testing the wine great than a specific value
                # print i
                for i in r[0]['descriptions']:
                    description.append(i)
        print result
        # print wines, id_w
        return wines, id_w, description

    def findTopRated(self, user_id):
        tx = self.g.cypher.begin()
        query = (
            'MATCH (u1:User {user_id:{user_id}})-[r:has_rated]->(w:Wine) WHERE r.rating >= 4 return w.description as res limit 2')
        tx.append(query, {'user_id': user_id})
        result = tx.commit()

        if len(result[0]) == 0:
            return []

        descript = []
        for r in result:
            for des in r:
                print des['res']
                descript.append(des['res'])

        return descript

    def findVarietyPrefer(self, user_id):
        threshold = 0
        query = "match (u:User{user_id:{user_id}})-[:has_rated]->()-[r:Is_variety]->(c:category) " \
                "with count(r) as counttype, c  " \
                "where counttype>{threshold} return collect(c.variety) as variety"

        tx = self.g.cypher.begin()
        tx.append(query, {'user_id': user_id, 'threshold': threshold})
        result = tx.commit()
        if len(result) is 0:
            return []
        else:
            return result

    # this function allows us to update the value of rating of each wine. we rely on Weighted welford algorithm to give major importance to the oldest avg
    def updateRating(self, wine_id, score):
        weightedNew = 0.15
        weightedOld = 0.85
        sumWeighted = weightedNew + weightedOld
        tx = self.g.cypher.begin()
        updateRatingWine = "MATCH (w:Wine{ wine_id:{wine_id}}) return w.rating as exRating"
        tx.append(updateRatingWine, {'wine_id': wine_id})
        result = tx.commit()
        meanOld = 0
        for r in result:
            if len(r) == 0:
                break
            else:
                meanOld = r[0]['exRating']

        newAvg = "MATCH (a:Wine{wine_id:{wine_id}})" \
                 "SET a.rating = {rating} RETURN a"
        # update the avg with welford algorithm, the last avg is more weighted than newest with the below parameters: weightedNew = 0.15 weightedOld = 0.85
        # print meanOld
        mean = meanOld + (weightedNew / sumWeighted) * (float(score) - meanOld)
        # print mean
        tx = self.g.cypher.begin()
        tx.append(newAvg, {'wine_id': wine_id, 'rating': mean})
        tx.commit()

    def getTopRated(self, user_id, minRating):
        tx = self.g.cypher.begin()
        query = 'MATCH (u:User{user_id:{U}})-[r:has_rated]->(w:Wine) WHERE r.rating >= {min} RETURN w.description as des, w.name as name LIMIT 2'
        tx.append(query, {'U': user_id, 'min': minRating})
        res = tx.commit()
        return res

    def getRating(self, user_id):
        tx = self.g.cypher.begin()
        query = 'MATCH (u:User{user_id:{U}})-[r:has_rated]->(w:Wine)' \
                'RETURN COUNT(r) as numRating, u.tastes as tastes'
        tx.append(query, {'U': user_id})
        res = tx.commit()
        print res
        result = 0
        tastes = ''
        for r in res:
            if len(r) == 0:
                break
            else:
                result = r[0]['numRating']
                tastes = r[0]['tastes']
                return result, tastes

        return result, tastes


def findSimilarity(user):
    threshold = 0.5

    # In Strategy 1, the similarity between two users u1 and u2 is the proportion of wines they have in common
    # The score of one given wine m is the proportion of users similar to u1 who rated m

    query = (  ### Similarity normalization : count number of wines drink by u1 ###
        # Count Wines rated by u1 as countw
        'MATCH (u1:`User` {user_id:{user_id}})-[:`has_rated`]->(w:`Wine`) WITH count(w) as countw '
        ### Score normalization : count number of users who are considered similar to u1 ###
        # Retrieve all users u2 who share at least one wines with u1
        'MATCH (u1:`User` {user_id:{user_id}})-[:`has_rated`]->(w:`Wine`) '
        'MATCH (w)<-[r:`has_rated`]-(u2:`User`) WHERE NOT u2=u1'
        # Compute similarity
        ' WITH u2, countw, tofloat(count(r))/countw as sim WHERE sim>{threshold} '
        # Keep users u2 whose similarity with u1 is above some threshold
        'WITH count(u2) as countu, countw '
        'MATCH (u1:`User` {user_id:{user_id}})-[:`has_rated`]->(w:`Wine`) '
        'MATCH (w)<-[r:`has_rated`]-(u2:`User`) WHERE NOT u2=u1 WITH u1, u2,countu, tofloat(count(      r))/countw as sim WHERE sim>{threshold} '
        'MATCH (w:`Wine`)<-[r:`has_rated`]-(u2) WHERE NOT (w)<-[:`has_rated`]-(u1) RETURN DISTINCT w ,tofloat(count(r))/countu as score ORDER BY score DESC ')

    tx = graph.cypher.begin()
    tx.append(query, {'user_id': user, 'threshold': threshold})
    result = tx.commit()
    return result


def findSimilarityScore(user):
    threshold = 0.0
    query = (  ### Similarity normalization : count number of wine seen by u1 ###
        # Count Wines rated by u1 as countWineUser1
        'MATCH (w1:`Wine`)<-[:has_rated]-(u1:`User` {user_id:{user_id}}) '
        'WITH count(w1) as countWineUser1 '
        ### Recommendation ###
        # Retrieve all users u2 who share at least one Wine with u1
        'MATCH (u2:`User`)-[r2:`has_rated`]->(w1:`Wine`)<-[r1:`has_rated`]-(u1:`User` {user_id:{user_id}}) '
        # Check if the ratings given by u1 and u2 differ by less than 1
        'WHERE (NOT u2=u1) AND (abs(r2.rating - r1.rating) <= 1) '
        # Compute similarity
        'WITH u1, u2, tofloat(count(DISTINCT w1))/countWineUser1 as sim '
        # Keep users u2 whose similarity with u1 is above some threshold
        'WHERE sim>{threshold} '
        # Retrieve Wines w that were rated by at least one similar user, but not by u1
        'MATCH (w:`Wine`)<-[r:`has_rated`]-(u2) '
        'WHERE (NOT (w)<-[:`has_rated`]-(u1)) '
        # Compute score and return the list of suggestions ordered by score
        'WITH DISTINCT w, count(r) as n_u, tofloat(sum(r.rating)) as sum_r '
        'WHERE n_u >= 1 '
        'RETURN w, sum_r/n_u as score ORDER BY score DESC')

    tx = graph.cypher.begin()
    tx.append(query, {'user_id': user, 'threshold': threshold})
    result = tx.commit()
    return result


def createSuperUserRating():
    pass


def main():
    wines = pd.read_csv(pathCSV + 'DB.csv', sep=',', header=None,
                        names=['id', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol',
                               'winery'])
    wines = wines.drop(['location', 'type', 'alchol', 'winery', 'price'], axis=1)

    genre = pd.read_csv(pathCSV + 'category.csv', sep=',', header=None, names=['id', 'category'])

    # createBasedCategories(genre)
    # createBasedWine(wines, genre)
    # createBaseUser()
    createSuperUserRating()
    GCF = GraphCollaborativeFilter()
    GCF.createBasedCategories(genre)
    GCF.createBasedWine(wines, genre)
    # GCF.createBaseUser()
    GCF.UpdateWineBase(wines)
    graph.cypher.execute('CREATE INDEX ON :User(user_id)')
    graph.cypher.execute('CREATE INDEX ON :Wine(wine_id)')
    graph.cypher.execute('CREATE INDEX ON :category(cat_id)')
    #
    # GCF.addNewUserRating(3, "mariagrazia", 2, 5)
    # GCF.addNewUserRating(3, "mariagrazia", 4, 5)
    # GCF.addNewUserRating(3, "mariagrazia", 3, 5)
    # GCF.addNewUserRating(3, "mariagrazia", 1, 5)
    # GCF.addNewUserRating(2, "ciccio", 3, 4)
    # GCF.addNewUserRating(4, "Simone", 1, 4)
    # GCF.addNewUserRating(4, "Simone", 2, 5)
    # GCF.addNewUserRating(4, "Simone", 7, 2)
    # GCF.addNewUserRating(4, "Simone", 4, 4)

    # print GCF.getWine(3)
    # x = GCF.getTopRated(1, 5)
    # for r in x:
    #     if len(r) == 0:
    #         break
    #     else:
    #         for d in r:
    #             print d['des']
    #             print "***"

    # adv = findSimilarity(1)
    # print adv

    # adv = findSimilarityScore(1)
    # print adv

    # adv = GCF.findSimilarityJaccard(4)
    # print adv

    # res = GCF.findTopRated(4)
    # print res


if __name__ == '__main__':
    main()
