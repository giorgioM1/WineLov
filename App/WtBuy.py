from google import google
import sys
reload(sys)
sys.setdefaultencoding('utf8')

# This class named "whereTobuy" implements a research of the wine's name in a google research, and respond with n_link in function our desire. we can read even the description and the link's name


class whereToBuy:
    def __init__(self, num_page, max_output):
        self.num_page = num_page
        self.max_output = max_output
        self.google = google

    def search(self, query):
        search_result = self.google.search(query, self.num_page)
        result = search_result[0:self.max_output]
        link = []
        description = []
        name = []
        for r in result:
            link.append(r.link)
            description.append(r.description)
            name.append(r.description)

        return link, name, description


def main():
    asker = whereToBuy(1, 7)
    q = 'Abbazia di Novacella 2014 Pinot Grigio'
    res = asker.search(q)
    for x in res[0]:
        print x


if __name__ == '__main__':
    main()

