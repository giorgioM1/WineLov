import collections
import json
import re
import nltk
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import word_tokenize
from sklearn.cluster import KMeans, DBSCAN
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer

path = "/home/giorgio/Scrivania/WineLov/DB/"
pathModel = "/home/giorgio/Scrivania/WineLov/ModelK/"


def getWine(fileJDB):
    wine = []
    # for i in range(0,len(fileJDB)):
    # print fileJDB[i]["name"]
    for i in range(0, len(fileJDB)):
        wine.append(fileJDB[i]["name"])
    return wine


def getDescription(fileJDB):
    description = []
    #    for i in range(0,len(fileJDB)):
    for i in range(0, len(fileJDB)):
        description.append(fileJDB[i]["description"])
    return description


def getVariety(fileJDB):
    variety = []
    for i in range(0, len(fileJDB)):
        variety.append(fileJDB[i]["variety"])
    return variety


def clean(fileJDB):
    i = 0
    while True:
        try:
            if fileJDB[i]["description"] is None:
                del fileJDB[i]
            else:
                i += 1
        except:
            print "finish"
            break
    return fileJDB


class clustering:
    def __init__(self, wine, description, variety, n_cluster):
        self.winelist = wine
        self.descriptionlist = description
        self.varietylist = variety
        self.vocab = None
        self.matrix = None
        self.vectorized = None
        self.cluster = n_cluster
        self.KM = None
        self.maxDf = 0.70
        self.minDf = 0.02

    def createVocab(self):
        voctokenizeDescriptionStemmed = []
        voctokenizeDescription = []
        for value in self.descriptionlist:
            stemmedword = tokenize_and_stem_and_stopword(value)
            voctokenizeDescriptionStemmed.extend(stemmedword)
            tokenizeWord = tokenize_only(value)
            voctokenizeDescription.extend(tokenizeWord)

        vocab_frame = pd.DataFrame({'words': voctokenizeDescription}, index=voctokenizeDescriptionStemmed)
        self.vocab = vocab_frame

    def getMatrix(self):
        # setting vectorized of the description
        tfidf_vectorizer = TfidfVectorizer(max_df=self.maxDf, max_features=300000,
                                           min_df=self.minDf, stop_words='english',
                                           use_idf=True, tokenizer=tokenize_and_stem_and_stopword, ngram_range=(1, 3))

        tfidf_matrix = tfidf_vectorizer.fit_transform(self.descriptionlist)

        self.matrix = tfidf_matrix
        self.vectorized = tfidf_vectorizer

    def setMaxMinDf(self, max, min):
        self.maxDf = max
        self.minDf = min

    def getVectorizer(self):
        return self.vectorized

    def setKmeans(self):
        self.KM = KMeans(n_clusters=self.cluster)


    def getKmeans(self):
        if self.KM is not None:
            return self.KM

    def fitting(self):
        self.KM.fit(self.matrix)

    def getlabels(self):
        return self.KM.labels_.tolist()

    def exportModel(self, km, name):
        # export model
        joblib.dump(km, pathModel + name + '.pkl')
        print 'save model with name: ' + str(name)

    def importModel(self, name):
        # import model
        model = joblib.load(pathModel + name + '.pkl')
        return model

    def getCentroid(self):
        return self.KM.cluster_centers_

    def showCluster(self, frame, terms):
        values = frame['clusters'].value_counts()
        print values

        print("Top terms per cluster:")
        # sort cluster centers by proximity to centroid
        order_centroids = self.KM.cluster_centers_.argsort()[:, ::-1]

        for i in range(self.cluster):
            print "Cluster %s words:" % i

            for ind in order_centroids[i, :10]:  # replace 6 with n words per cluster
                print ' %s' % self.vocab.ix[terms[ind].split(' ')].values.tolist()[0][0].encode('utf-8', 'ignore')

            print "Cluster %d wines:" % i
            j = 0
            for name in frame.ix[i]['wines'].values.tolist():
                print name
                j += 1
                if j == 3:
                    break
            print "Cluster %d description:" % i
            j = 0
            for description in frame.ix[i]['description'].values.tolist():
                print description
                j += 1
                if j == 3:
                    break

            print "Cluster %d variety:" % i
            lists = []
            for variety in frame.ix[i]['variety'].values.tolist():
                # print variety
                lists.append(variety)
            c = collections.Counter(lists)
            print c

    def prediction(self, vectorElement):
        return self.KM.predict(vectorElement)

    def feature(self):
        return self.vectorized.get_feature_names()


def tokenize_and_stem_and_stopword(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    stems = []
    stemmer = SnowballStemmer("english")
    tokens = word_tokenize(text)
    # lower case
    tokens = [w.lower() for w in tokens]
    # remove stop words
    stop_words = set(stopwords.words('english'))
    stop_words.update(['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', "it'll"])
    words = [w for w in tokens if not w in stop_words]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in words:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)

    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    stop_words = set(stopwords.words('english'))
    words = [w for w in tokens if not w in stop_words]
    filtered_tokens = []

    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in words:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens


def showCluster(frame, vocab_frame, km, num_clusters, terms):
    values = frame['clusters'].value_counts()
    print values

    print("Top terms per cluster:")
    # sort cluster centers by proximity to centroid
    order_centroids = km.cluster_centers_.argsort()[:, ::-1]

    for i in range(num_clusters):
        print "Cluster %s words:" % i

        for ind in order_centroids[i, :10]:  # replace 6 with n words per cluster
            print ' %s' % vocab_frame.ix[terms[ind].split(' ')].values.tolist()[0][0].encode('utf-8', 'ignore')

        print "Cluster %d wines:" % i
        j = 0
        for name in frame.ix[i]['wines'].values.tolist():
            print name
            j += 1
            if j == 3:
                break
        print "Cluster %d description:" % i
        j = 0
        for description in frame.ix[i]['description'].values.tolist():
            print description
            j += 1
            if j == 3:
                break

        print "Cluster %d variety:" % i
        lists = []
        for variety in frame.ix[i]['variety'].values.tolist():
            # print variety
            lists.append(variety)
        c = collections.Counter(lists)
        print c


def main():
    x = open(path + "DB.json", mode="r").read()
    jDB = json.loads(x)
    print len(jDB)
    jDB = clean(jDB)
    print "exit"
    wine = getWine(jDB)
    description = getDescription(jDB)
    variety = getVariety(jDB)
    print len(description)

    clusteringAlg = clustering(wine, description, variety, 10)

    clusteringAlg.createVocab()
    clusteringAlg.getMatrix()
    clusteringAlg.setKmeans()
    clusteringAlg.fitting()
    terms = clusteringAlg.feature()
    print terms
    clusters = clusteringAlg.getlabels()

    wines = {'wines': wine, 'description': description, 'clusters': clusters, 'variety': variety}
    frame = pd.DataFrame(wines, index=[clusters], columns=['wines', 'description', 'variety', 'clusters'])

    clusteringAlg.showCluster(frame, terms)

    # x = []
    # print "new value"
    # print jDB[16000]["description"]
    # x.append(jDB[16000]["description"])
    # # convert a description to vector to confront with other clusters
    # Nd = tfidf_vectorizer.transform(x)
    # # print terms
    # print Nd
    # print "prediction" + str(km.predict(Nd))


if __name__ == '__main__':
    main()
