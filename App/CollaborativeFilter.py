import sys
import csv
import numpy as np
import re
import pandas as pd
import json
import redis
from numpy.linalg import matrix_rank
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from scipy.sparse.linalg import svds

import math
import scipy.sparse as sp
from scipy.sparse.linalg import svds
from collections import Counter

from sklearn import cross_validation as cv
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics import mean_squared_error
from math import sqrt

reload(sys)
sys.setdefaultencoding('utf8')

pathDB = "/home/giorgio/Scrivania/WineLov/DB/"
pathCSV = "/home/giorgio/Scrivania/WineLov/CSV/"


def predict(ratings, similarity, type='user'):
    if type == 'user':
        mean_user_rating = ratings.mean(axis=1)
        # You use np.newaxis so that mean_user_rating has same format as ratings
        ratings_diff = (ratings - mean_user_rating[:, np.newaxis])
        pred = mean_user_rating[:, np.newaxis] + similarity.dot(ratings_diff) / np.array(
            [np.abs(similarity).sum(axis=1)]).T
    elif type == 'item':
        pred = ratings.dot(similarity) / np.array([np.abs(similarity).sum(axis=1)])
    return pred


def rmse(prediction, ground_truth):
    prediction = prediction[ground_truth.nonzero()].flatten()
    ground_truth = ground_truth[ground_truth.nonzero()].flatten()
    return sqrt(mean_squared_error(prediction, ground_truth))


def refactorCsv():
    indexForSurvey = []
    set = []
    name = []
    variety = []

    with open(pathCSV + "ratings.csv", 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_NONE)
        for row in reader:
            idx = 1
            for i in range(2, len(row)):
                # print row[i]
                d = re.sub("[!@#$']", '', row[i]).replace('"', '')
                d = d.strip().split('-')
                set.append(d[0].strip())
                name.append(d[1])
                variety.append(d[2])
                indexForSurvey.append(idx)
                idx += 1
            break

    print len(set), len(variety), len(name)
    for i in range(0, len(set)):
        with open(pathCSV + "newSurvey.csv", "a") as fw:
            writer = csv.writer(fw, delimiter=',', quotechar='|')
            writer.writerow([set[i], name[i], variety[i], indexForSurvey[i]])

    with open(pathCSV + "ratings2.csv", 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        index_user = 1
        for row in reader:
            votes = []
            indexForSurvey = []
            idx = 1
            for i in range(2, len(row)):
                vote = row[i]
                votes.append(vote)
                indexForSurvey.append(idx)
                idx += 1

            # votes[0] = votes[0].replace("]", "")
            # votes[len(votes) - 1] = votes[len(votes) - 1].replace("]", '')
            for i in range(0, len(votes)):
                with open(pathCSV + "ratingUtoM.csv", "a") as csvW:
                    writer = csv.writer(csvW, delimiter=',', quotechar='|')
                    writer.writerow([index_user, set[i], votes[i], indexForSurvey[i]])
            index_user += 1

        # for row in reader:
        #     s = str(row).split(",")
        #     print len(s)
        #     user = s[1]
        #     ts = s[0]
        #
        #     print "++:" + s[5]
        #     if s[5] == " ''":
        #         print "novote"


def createSparseValue():
    indexForSurvey = []
    set = []
    name = []
    variety = []

    with open(pathCSV + "ratings.csv", 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_NONE)
        for row in reader:
            idx = 1
            for i in range(2, len(row)):
                # print row[i]
                d = re.sub("[!@#$']", '', row[i]).replace('"', '')
                d = d.strip().split('-')
                set.append(d[0].strip())
                name.append(d[1])
                variety.append(d[2])
                indexForSurvey.append(idx)
                idx += 1
            break

    print len(set), len(variety), len(name)
    for i in range(0, len(set)):
        with open(pathCSV + "secondMap.csv", "a") as fw:
            writer = csv.writer(fw, delimiter=',', quotechar='|')
            writer.writerow([set[i], name[i], variety[i], indexForSurvey[i]])

    with open(pathCSV + "ratings2.csv", 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        index_user = 1
        for row in reader:
            votes = []
            indexForSurvey = []
            idx = 1
            for i in range(2, len(row)):
                if row[i] is not '':
                    votes.append(row[i])
                    indexForSurvey.append(idx)
                    idx += 1
                else:
                    idx += 1
            for i in range(0, len(votes)):
                with open(pathCSV + "sparseUtoM.csv", "a") as csvW:
                    writer = csv.writer(csvW, delimiter=',', quotechar='|')
                    writer.writerow([index_user, set[indexForSurvey[i] - 1], votes[i], indexForSurvey[i]])
            index_user += 1


class collaborativeFiltering:
    def __init__(self, ratings, n=5):
        self.n = n
        self.reshaped = None
        self.sparseRating = ratings
        self.UtoR = None
        self.UtoRDF = None
        self.U = None
        self.sigma = None
        self.Vt = None
        self.usr_rating_mean = None
        self.preds_df = None
        self.createMatrix()
        # make mean user wine ratings
        self.meanMatrix()
        # reformat the matrix UtoW less the mean values
        self.reshapedMatrix(self.usr_rating_mean)
        # calculate svd with latent factors equals to reshaped.shape-1
        self.svd()
        # calculate the rating predict for each wine for each user
        predit = self.calculatePredictedRating()
        # create dataframe for predict
        self.createPredictDataFrame(predit)

    def recommend_wine(self, predictions_df, user_id, wine_df, original_ratings_df, num_recommendations=5):
        # Get and sort the user's predictions
        user_row_number = user_id - 1  # UserID starts at 1, not 0
        sorted_user_predictions = predictions_df.iloc[user_row_number].sort_values(ascending=False)

        # Get the user's data and merge in the wine information.
        user_data = original_ratings_df[original_ratings_df.user_id == (user_id)]
        user_full = (user_data.merge(wine_df, how='left', left_on='wine_id', right_on='wine_id').sort_values(['rating'],
                                                                                                             ascending=False))

        print 'User {0} has already rated {1} movies.'.format(user_id, user_full.shape[0])
        print 'Recommending the highest {0} predicted ratings movies not already rated.'.format(num_recommendations)

        # Recommend the highest predicted rating movies that the user hasn't seen yet.
        recommendations = (wine_df[~wine_df['wine_id'].isin(user_full['wine_id'])].
                               merge(pd.DataFrame(sorted_user_predictions).reset_index(), how='left',
                                     left_on='wine_id',
                                     right_on='wine_id').
                               rename(columns={user_row_number: 'Predictions'}).
                               sort_values('Predictions', ascending=False).
                               iloc[:num_recommendations])

        return user_full, recommendations

    def recommendToUser(self, user, wine):
        voted, predictedWine = self.recommend_wine(self.preds_df, user, wine, self.sparseRating, self.n)
        return voted, predictedWine

    def createMatrix(self):
        self.UtoRDF = self.sparseRating.pivot(index='user_id', columns='wine_id', values='rating').fillna(0)
        # print self.UtoRDF
        self.UtoR = self.UtoRDF.as_matrix()

    def meanMatrix(self):
        self.usr_rating_mean = np.mean(self.UtoR, axis=1)
        return self.usr_rating_mean

    def reshapedMatrix(self, user_ratings_mean):
        self.reshaped = self.UtoR - user_ratings_mean.reshape(-1, 1)

    def svd(self):
        # self.U, sigma, self.Vt = svds(self.reshaped, k=self.reshaped.shape[0] - 1)
        r = matrix_rank(np.array(self.reshaped))
        print "rank: " + str(r)
        self.U, sigma, self.Vt = svds(self.reshaped, k=r)
        self.sigma = np.diag(sigma)

    def getMatrixSvd(self):
        return self.U, self.sigma, self.Vt

    def calculatePredictedRating(self):
        return np.dot(np.dot(self.U, self.sigma), self.Vt) + self.usr_rating_mean.reshape(-1, 1)

    def createPredictDataFrame(self, all_user_predicted_ratings):
        self.preds_df = pd.DataFrame(all_user_predicted_ratings, columns=self.UtoRDF.columns)
        # print self.preds_df


def main():
    pass
    # header = ['timestamp', 'user_id', 'wine_id', 'rating', 'index']
    # allrating = pd.read_csv(pathCSV + "ratingUtoM.csv", sep=',', names=header, low_memory=False)
    # sparse = pd.read_csv(pathCSV + "ratingFromUser.csv", sep=',', names=header, low_memory=False,
    #                      dtype=int).sort_values(['timestamp'])
    # sparse = sparse.drop(['timestamp', 'index'], axis=1)
    # dfsparse = sparse.drop_duplicates(['user_id', 'wine_id'], keep='first')
    # headerWine = ['wine_id', 'name', 'variety', 'index']
    # wine = pd.read_csv(pathCSV + "wineCF.csv", sep=',', names=headerWine, low_memory=False)
    # wine = wine.drop(['index'], axis=1)
    #
    # X_train, X_test = train_test_split(dfsparse, test_size=0.2)
    #
    # CF = collaborativeFiltering(dfsparse, n=20)
    #
    #
    # voted, predictedWine = CF.recommendToUser(1, wine)


if __name__ == '__main__':
    main()
