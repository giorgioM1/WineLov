import csv
import time

from threading import Event, Thread

from flask import Flask
from flask import json
from flask import request
import json
from json import dumps
import pandas as pd

from update import UpdateModel
from LSA import LSASimilarity
from CollaborativeFilter import collaborativeFiltering
from GraphCollaborativeFiltering import GraphCollaborativeFilter
from WineClassification import LabelClassification
from WtBuy import whereToBuy
from similarityDesc import similarity
from ConnectionDB import createConnectionDB
from matchingQuestion import matchingAnswer
from foodClassifier import oneVSAllClassifier
import sys

reload(sys)
sys.setdefaultencoding('utf8')

app = Flask(__name__)

pathCSV = "/home/giorgio/Scrivania/WineLov/CSV/"

afterN = 1
users = 2000
N = 100
Model = False
MaxDelay = 30

header = ['timestamp', 'user_id', 'wine_id', 'rating', 'index']
sparse = pd.read_csv(pathCSV + "ratingFromUser.csv", sep=',', names=header, low_memory=False, dtype=int).sort_values(
    ['timestamp'])
sparse = sparse.drop(['timestamp'], axis=1)
sparse = sparse.drop_duplicates(['user_id', 'wine_id'], keep='first')
headerWine = ['wine_id', 'name', 'variety', 'index']
wine = pd.read_csv(pathCSV + "wineCF.csv", sep=',', names=headerWine, low_memory=False)
headerWine = ['id_wine', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']
df = pd.read_csv(pathCSV + 'DB.csv', sep=',', header=None, names=headerWine)

t0 = time.time()
print "init collaborative filtering"
CF = collaborativeFiltering(sparse)
t1 = time.time()
print "init graph collaborative filtering"
GCF = GraphCollaborativeFilter()
t2 = time.time()
print "init similarity with Red Wines"
CBFR = similarity(pathCSV + "DBRed.csv", 0.25, 1)
t3 = time.time()
print "init similarity with White Wines"
CBFW = similarity(pathCSV + "DBWhite.csv", 0.25, 1)
t4 = time.time()
print "init label Classification to verify if a wine is Red o White"
LSA = LabelClassification(df, "Red")
t5 = time.time()
print "init food classification label"
foodMatching = oneVSAllClassifier()
t6 = time.time()

LSAw = LSASimilarity(pathCSV + "DBWhite.csv")
LSAr = LSASimilarity(pathCSV + "DBRed.csv")
t7 = time.time()

WTB = whereToBuy(num_page=1, max_output=6)

print "collaborative filtering SVD time: " + str(t1 - t0)
print "graph collaborative filtering: " + str(t2 - t1)
print "similarity Red: " + str(t3 - t2)
print "similarity White: " + str(t4 - t3)
print "labelClassification: " + str(t5 - t4)
print "foodMatching: " + str(t6 - t5)
print "Latent Semantic Analysis: " + str(t7 - t6)

db = createConnectionDB()
mtcAnsw = matchingAnswer()

idTop = []


@app.route('/')
def hello_world():
    return "ok"


def createResult(predictedWine):
    M = predictedWine.as_matrix()
    id = []
    name = []
    variety = []
    data = {}
    re = []
    for i in range(0, len(M)):
        id.append(M[i][0])
        name.append(M[i][1])
        variety.append(M[i][2])
    data['id'] = id
    data['name'] = name
    data['variety'] = variety
    re['CF'] = data
    return json.dumps(re)


@app.route('/getRecommendation', methods=['POST'])
def getAllRecommendation():
    global Model
    data = request.json
    user_id = data['user']
    n, tastes = GCF.getRating(user_id)
    # first level
    if n < 5:
        # get wines for our tastes
        wgt = getReccomandationTastes(tastes, 20)
        # print name of wines
        x = json.loads(wgt)
        for i in x:
            print i['name']
        return wgt
    else:
        if Model is True:
            # third level
            voted, predictedWine = CF.recommendToUser(user_id, wine)
            result = createResult(predictedWine)
            taste_rec = getTasteTips(user_id)
            result['CB'] = taste_rec
            return result
        else:
            # secondo level
            t0 = time.time()
            wineAdv, ids, descriptions = GCF.findSimilarityJaccard(user_id)
            t1 = time.time()
            dim = CF.sparseRating.shape[0]
            mean = dim / users
            print mean
            if (t1 - t2) > MaxDelay:
                # Model = True
                pass
            if mean > 25:
                Model = True

            if len(wineAdv) != 0:
                result = {}
                print wineAdv
                print ids
                print descriptions
                links = []
                foods = []
                for w in wineAdv:
                    link = WTB.search(w)[0]
                    print link
                    links.append(link)
                for d in descriptions:
                    food = foodMatching.oneVSallPredict(d)
                    print food
                    foods.append(food)
                result['id'] = ids
                result['wine'] = wineAdv
                result['description'] = descriptions
                result['link'] = links
                result['food'] = foods

                return json.dumps(result)
            else:
                return json.dumps({'id': '', 'name': '', 'description': ''})


def getWineSimilarity(user_id):
    t0 = time.time()
    wineAdv, ids, descriptions = GCF.findSimilarityJaccard(user_id)
    t1 = time.time()
    voted, predictedWine = CF.recommendToUser(user_id, wine)
    t2 = time.time()
    print "Performace: GFC " + str(t1 - t0) + " CF " + str(t2 - t1)
    print voted
    print predictedWine
    if len(wineAdv) != 0:
        print wineAdv
        print ids
        print descriptions
        links = []
        foods = []
        for w in wineAdv:
            link = WTB.search(w)[0]
            print link
            links.append(link)
        # for d in descriptions:
        #     food = foodMatching.oneVSallPredict(d)
        #     print food
        #     foods.append(food)

        return wineAdv, links, ids
    else:
        return [[], [], []]
    # return wineAdv, links, foods


@app.route('/foodMatching/<int:wine_id>', methods=['GET'])
def findFood(wine_id):
    des = GCF.getWine(wine_id)
    print des
    food = foodMatching.oneVSallPredict(des)
    print food
    return json.dumps(food)


# this function maps the answers of the question
def createList(json):
    ansW = []
    # print json['1']
    ansW.append(str(json['user']))
    ansW.append(str(json['1']))
    ansW.append(str(json['2']))
    ansW.append(str(json['3']))
    ansW.append(str(json['4']))
    ansW.append(str(json['5']))
    ansW.append(str(json['6']))
    ansW.append(str(json['7']))
    ansW.append(str(json['8']))
    ansW.append(str(json['9']))
    ansW.append(str(json['10']))
    return ansW


@app.route('/getTastes', methods=['POST'])
def api_message():
    print request.json
    if request.headers['Content-Type'] == 'application/json':
        # crate list from response
        tastes = createList(request.json)
        # print tastes
        tastes = mtcAnsw.parseAnswerSimple(tastes)
        print tastes
        # get wines for our tastes
        wgt = getReccomandationTastes(tastes, 20)
        # print name of wines
        x = json.loads(wgt)
        # print x
        for i in x:
            print i['name']
        return wgt


# to add new user and save their taste
@app.route('/addUserNew', methods=['POST'])
def newUserNew():
    if request.headers['Content-Type'] == 'application/json':
        data = request.json
        tastes = data['tastes']
        print tastes
        tastes = createList(tastes)
        tastes = mtcAnsw.parseAnswerSimple(tastes)

        if GCF.addNewUserNew(data['name'], tastes) is True:
            print "save"
            return json.dumps(True)
        else:
            print "error saving new rating"
            return json.dumps(False)


# @app.route('/addUser', methods=['POST'])
# def newUser():
#     if request.headers['Content-Type'] == 'application/json':
#         data = request.json
#         if GCF.addNewUser(data['name']) is True:
#             print "save"
#             return json.dumps(True)
#         else:
#             print "error saving new rating"
#             return json.dumps(False)

# This function allow us to save a new rating
@app.route('/addRating', methods=['POST'])
def newRating():
    global afterN
    global CF
    if request.headers['Content-Type'] == 'application/json':
        data = request.json
        print data['user_id'], data['wine_id'], data['rating']
        ts = int(time.time())
        with open(pathCSV + 'ratingFromUser.csv', 'a') as csvfile:
            writer = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
            writer.writerow([ts, data['user_id'], data['wine_id'], data['rating'], data['wine_id']])

        ready = Event()
        updateCF = UpdateModel(ready)

        if (afterN % 50) == 0:
            print "RECALCULATE MODEL"
            thread = Thread(target=updateCF.make)
            thread.start()
            ready.wait()
            CF = updateCF.newModel()
        afterN += 1

        if GCF.addNewRatings(data['user_id'], data['wine_id'], data['rating']) is True:
            print "save"
            return json.dumps(True)
        else:
            print "error saving new rating"
            return json.dumps(False)


# This function take the user_id and found the wine with more similarity
@app.route('/getTasteTips/<int:user_id>', methods=['GET'])
def getTasteTips(user_id):
    Glink = True
    minRating = 5
    rs = GCF.getTopRated(user_id, minRating)
    res = []
    for r in rs:
        if len(r) == 0:
            return json.dumps({})
        else:
            for d in r:
                wine_des = d['des']
                print d['name']
                if LSA.predict(wine_des) is False:
                    print "White"
                    resp = LSAw.getNeighbours(LSAw.transformDes(wine_des)[0])
                    resp = json.loads(resp)
                    print resp
                    if Glink is True:
                        for element in resp['name']:
                            print element
                            link = WTB.search(element)[0]
                            resp['link_' + element] = link
                    res.append(resp)
                else:
                    print "Red"
                    resp = LSAr.getNeighbours(LSAr.transformDes(wine_des)[0])
                    resp = json.loads(resp)
                    print resp
                    if Glink is True:
                        for element in resp['name']:
                            print element
                            link = WTB.search(element)[0]
                            resp['link_' + element] = link
                    res.append(resp)

    return json.dumps(res)


# this function allows us to fìnd out the links where to buy the wine
@app.route('/whereToBuy', methods=['POST'])
def whereBuyWine():
    if request.headers['Content-Type'] == 'application/json':
        data = request.json

    res = {}
    links = WTB.search(data['name'])[0]
    res['links'] = links

    return json.dumps(res)

# this function takes in input the description and find out
# and predicts if the wine is white or red and find the most similar wine
@app.route('/getCloseWine', methods=['POST'])
def getCloseWine():
    if request.headers['Content-Type'] == 'application/json':
        data = request.json
        print data
        print data['user_id'], data['wine_id'], data['description']

    res = []
    if LSA.predict(data['description']) is False:
        print "White"
        resp = LSAw.getNeighbours(LSAw.transformDes(data['description'])[0])
        resp = json.loads(resp)
        print resp
        res.append(resp)
    else:
        print "Red"
        resp = LSAr.getNeighbours(LSAr.transformDes(data['description'])[0])
        resp = json.loads(resp)
        print resp
        res.append(resp)
    return json.dumps(res)


def getReccomandationTypeTastes(tastes, hn, type):
    rs = []
    jsw = None
    jsr = None
    if type is "White":
        w = CBFW.get_recommendations(tastes, hn)
        jsw = json.loads(w)

    else:
        r = CBFR.get_recommendations(tastes, hn)
        jsr = json.loads(r)

    w_id = []
    # get item from db
    if jsw is None:
        for i in range(0, len(jsr)):
            rx = db.get(jsr[i] - 1)
            w_id.append(jsr[i])
            rs.append(json.loads(rx))
    else:
        for i in range(0, len(jsw)):
            rx = db.get(jsw[i] - 1)
            w_id.append(jsw[i])
            rs.append(json.loads(rx))
    print set(w_id)
    for r in rs:
        print r
    return json.dumps(rs)


# @app.route('/reccYourTaste/<string:tastes>')
def getReccomandationTastes(tastes, hn):
    rs = []
    # get 5 wines for type
    r = CBFR.get_recommendations(tastes, hn)
    w = CBFW.get_recommendations(tastes, hn)

    jsr = json.loads(r)
    jsw = json.loads(w)
    w_id = []
    # get item from db
    for i in range(0, len(jsr)):
        rx = json.loads(db.get(jsr[i] - 1))
        rx['id'] = jsr[i]
        w_id.append(jsr[i])
        # print rx
        rs.append(rx)
    for i in range(0, len(jsw)):
        rx = json.loads(db.get(jsw[i] - 1))
        rx['id'] = jsw[i]
        w_id.append(jsw[i])
        rs.append(rx)
    return json.dumps(rs)


@app.route('/getSimilarity/<int:user_id>')
def getSimilarity(user_id):
    # todo add a method that match some wines for tastes and we make union for similarity
    return dumps(getWineSimilarity(user_id))


@app.route('/getMaxSimilarity/<int:user_id>')
def getMaxSimilarity(user_id):
    descrs = GCF.findTopRated(user_id)
    secondScore = []
    for ds in descrs:
        if LSA.predict(ds) == "Red":
            rs = getReccomandationTypeTastes(ds, 3, "Red")
        else:
            rs = getReccomandationTypeTastes(ds, 3, "White")
        rs = json.loads(rs)
        for r in rs:
            secondScore.append(r)

    return dumps(secondScore)


@app.route('/getPrefVariety/<int:user_id>')
def getVarietyPrefer(user_id):
    variety = GCF.findVarietyPrefer(user_id)
    res = variety
    if len(res[0]) == 0:
        return []
    varieties = []
    for r in res:
        if len(r) == 0:
            break
        else:
            for v in r[0]['variety']:
                print v
                varieties.append(v)

    print varieties

    return json.dumps(varieties)


def main():
    app.run("localhost", 8080)


if __name__ == '__main__':
    main()
