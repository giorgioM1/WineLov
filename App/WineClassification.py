from sklearn.cross_validation import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn.neighbors import KNeighborsClassifier

import pandas as pd

from CollaborativeFilter import pathCSV



# we pass even the type of the classification beacuse we can use it to classify the food, and the n_typecls is the category of the food
from WitheWineClustering import tokenize_and_stem_and_stopword


class LabelClassification:
    def __init__(self, ds, n_typeCls):
        self.dataset = ds
        self.type = n_typeCls
        self.trainSet = None
        self.testSet = None
        self.labelTrain = None
        self.labelTest = None
        self.lsa = None
        self.max_df = 0.7
        self.min_df = 0.02
        self.matrix = None
        self.vectorized = None
        self.train_lsa = None
        self.classifier = None
        self.createDataSet()
        self.getMatrix()
        self.getLsa()
        self.LSAMatrix()
        self.classify()

    def __str__(self):
        print self

    def createDataSet(self):
        X_train, X_test, y_train, y_test = train_test_split(self.dataset['description'], self.dataset['type'],
                                                            random_state=0)
        self.trainSet = X_train
        self.testSet = X_test
        self.labelTrain = y_train
        self.labelTest = y_test

        self.labelTrain = [self.type in y for y in y_train]
        self.labelTest = [self.type in y for y in y_test]

    # the function creates the tf-idf matrix for the first step
    # return the vectorized model and the matrix
    def getMatrix(self):
        # setting vectorized of the description
        tfidf_vectorizer = TfidfVectorizer(max_df=self.max_df, min_df=self.min_df, max_features=300000,
                                           stop_words='english',
                                           use_idf=True,
                                           tokenizer=tokenize_and_stem_and_stopword,
                                           ngram_range=(1, 3), strip_accents='unicode')

        tfidf_matrix = tfidf_vectorizer.fit_transform(self.trainSet)

        # print tfidf_vectorizer.vocabulary_
        self.matrix = tfidf_matrix
        self.vectorized = tfidf_vectorizer

    # create the svd and Lsa object to reduce the dimension of the field
    def getLsa(self):
        svd = TruncatedSVD(100)
        self.lsa = make_pipeline(svd, Normalizer(copy=False))
        return self.lsa

    # train the lsa model with the matrix tf-idf
    def LSAMatrix(self):
        self.train_lsa = self.lsa.fit_transform(self.matrix)

    # trasform the text into lsa field
    def trasformLsa(self, text):
        x = self.vectorized.transform([text])
        x = self.lsa.transform(x)
        return x

    # define the classifier, we decided to use Knn as classifier, and we train it whit the LSA's matrix
    def classify(self):
        knn_lsa = KNeighborsClassifier(n_neighbors=5, algorithm='brute', metric='cosine')
        knn_lsa.fit(self.train_lsa, self.labelTrain)
        self.classifier = knn_lsa

    # take in input a text and predict which is your category
    def predict(self, des):
        x = self.trasformLsa(des)
        out = self.classifier.predict(x)
        print out
        if out == True:
            return self.type
        else:
            return False


def main():
    pass
    # header = ['id_wine', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']
    # df = pd.read_csv(pathCSV + 'DB.csv', sep=',', header=None, names=header)
    #
    # LC = LabelClassification(df, "Red")
    # # LC.createDataSet()
    # print("  %d training examples (%d Red)" % (len(LC.labelTrain), sum(LC.labelTrain)))
    # print("  %d test examples (%d Red)" % (len(LC.labelTest), sum(LC.labelTest)))
    #
    # # LC.getMatrix()
    # # print("  Actual number of tfidf features: %d" % LC.matrix.get_shape()[1])
    #
    # # lsa = LC.getLsa()
    # # X_train_lsa = lsa.fit_transform(LC.matrix)
    #
    # X_test_tfidf = LC.vectorized.transform(LC.testSet)
    # X_test_lsa = LC.lsa.transform(X_test_tfidf)
    #
    # # knn_lsa = KNeighborsClassifier(n_neighbors=5, algorithm='brute', metric='cosine')
    # # knn_lsa.fit(X_train_lsa, LC.labelTrain)
    #
    # # Classify the test vectors.
    # # lsaTrasformed = LC.trasformLsa(
    # # "Intoxicating scents of smoke, flint, dried mountain herb and woodland berry are just some of the multifaceted aromas you'll find on this dazzling Nebbiolo. It's extremely elegant, with a weightless intensity, delivering Marasca cherry, crushed strawberry, white pepper and tobacco framed in fine tannins. Vibrant acidity gives it impeccable balance while a flinty mineral vein energizes the finish. Some may find this almost too ethereal while lovers of finesse, complexity and balance will be enchanted. Drink through 2027.")
    #
    # p = LC.predict(
    #     "Just what you would expect from a $7 Pinot Grigio. Pleasant citrus and peach aromas and flavors are accented with a touch of almond. A crisp and simple wine")
    # print "classify of a white wine " + str(p)
    #
    # # testing the accuracy of the model
    # p = LC.classifier.predict(X_test_lsa)
    # # Measure accuracy
    # numRight = 0;
    # for i in range(0, len(p)):
    #     if p[i] == LC.labelTest[i]:
    #         numRight += 1
    #
    # print("  (%d / %d) correct - %.2f%%" % (
    #     numRight, len(LC.labelTest), float(numRight) / float(len(LC.labelTest)) * 100.0))


if __name__ == '__main__':
    main()
