#!/usr/bin/env python
# -*- coding: utf-8 -*-
import redis
import json


def createConnectionDB():
    r = redis.Redis(
        host="localhost",
        port=6379,
        db=0)
    return r

def createFileDB():
    r = redis.Redis(
        host="localhost",
        port=6379,
        db=0)

    raw = open("/home/giorgio/Scrivania/WineLov/DB/DB.json", mode="w+")
    dbsize = r.dbsize()
    DB = []
    i = 1
    for i in range(1, 15):
        data = json.loads(r.get(i))
        if data == None:
            break
        DB.append(data)
    raw.write(json.dumps(DB))
    raw.close()

    # x = open("DB.json", mode = "r").read()
    # j = json.loads(x)
    # print len(j)
    # print j[0]["name"]


def createFileDBType(type):
    r = redis.Redis(
        host="localhost",
        port=6379,
        db=0)
    filename = "DB" + str(type) + ".json"
    raw = open(filename, mode="w+")
    dbsize = r.dbsize()
    DB = []
    i = 1
    for i in range(1, dbsize):
        data = json.loads(r.get(i))
        if data == None:
            break
        elif data["type"] == type:
            DB.append(data)
    raw.write(json.dumps(DB))
    raw.close()


def main():
    createFileDB()
    # createFileDBType("White")
    # createFileDBType("Red")


if __name__ == '__main__':
    main()
