import pandas as pd
import sys
from io import StringIO
import matplotlib.pyplot as plt
import seaborn as sns

from IPython.display import display

import re
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn import metrics

from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import chi2
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score

reload(sys)
sys.setdefaultencoding('utf8')

pathDB = "/home/giorgio/Scrivania/WineLov/DB/"
pathCSV = "/home/giorgio/Scrivania/WineLov/CSV/"
pathModel = "/home/giorgio/Scrivania/WineLov/Model"


def main():
    mltClass = multiLabelClassification(pathCSV + 'DB.csv')
    var = mltClass.getDataFrame()


def tokenize_and_stem_and_stopword(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    stems = []
    # stemmer = SnowballStemmer("english")
    tokens = word_tokenize(text)
    # lower case
    tokens = [w.lower() for w in tokens]
    # remove stop words
    stop_words = set(stopwords.words('english'))
    stop_words.update(['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', "it'll"])
    words = [w for w in tokens if not w in stop_words]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in words:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens


class multiLabelClassification:

    def __init__(self, pathSourceFile):
        self.category_id_df = None
        self.fileSource = pathSourceFile
        self.dictCatToId = None
        self.dictIdToCat = None
        self.labels = None
        self.features = None
        self.X_train = None
        self.X_test = None
        self.Y_train = None
        self.Y_test = None
        self.wines = self.getDataFrame()
        self.plotCategory()
        self.tfidf = self.getMAtrixTFIDF()
        self.getFeature()
        self.getKeyWords(5)
        self.createDataSet()
        # self.multimodel()
        self.linearSVM()
        self.logisticRegression()
        self.randomForecast()

    def getDataFrame(self):
        header = ['id_wine', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']
        df = pd.read_csv(self.fileSource, sep=',', names=header, low_memory=False)
        # wines = df.drop(['location', 'type', 'alchol', 'winery', 'price','point'], axis=1)
        wines = df[pd.notnull(df['description'])]
        col = ['id_wine', 'name', 'description', 'variety']
        wines = wines[col]
        # print wines.columns
        wines['category_id'] = wines['variety'].factorize()[0]
        # print wines
        category_id_df = wines[['variety', 'category_id']].drop_duplicates().sort_values('category_id')
        self.category_id_df = category_id_df
        category_to_id = dict(category_id_df.values)
        self.dictCatToId = category_to_id
        id_to_category = dict(category_id_df[['category_id', 'variety']].values)
        self.dictIdToCat = id_to_category
        # return dataframe
        return wines

    def plotCategory(self):
        fig = plt.figure(figsize=(30, 28))
        self.wines.groupby('variety').id_wine.count().plot.bar(ylim=0)
        plt.savefig("plot")

    def getMAtrixTFIDF(self):
        tfidf = TfidfVectorizer(max_df=0.60, max_features=300000,
                                min_df=100, stop_words='english',
                                use_idf=True,
                                norm='l2',
                                tokenizer=tokenize_and_stem_and_stopword,
                                ngram_range=(1, 3), strip_accents='unicode')
        return tfidf

    def getFeature(self):
        self.features = self.tfidf.fit_transform(self.wines.description).toarray()
        self.labels = self.wines.category_id
        print self.features.shape

    def getKeyWords(self, howManyWords):
        N = howManyWords
        for Product, category_id in sorted(self.dictCatToId.items()):
            features_chi2 = chi2(self.features, self.labels == category_id)
            indices = np.argsort(features_chi2[0])
            feature_names = np.array(self.tfidf.get_feature_names())[indices]
            unigrams = [v for v in feature_names if len(v.split(' ')) == 1]
            bigrams = [v for v in feature_names if len(v.split(' ')) == 2]
            trigrams = [v for v in feature_names if len(v.split(' ')) == 3]
            # print("# '{}':".format(Product))
            # print("  . Most correlated unigrams:\n       . {}".format('\n       . '.join(unigrams[-N:])))
            # print("  . Most correlated bigrams:\n       . {}".format('\n       . '.join(bigrams[-N:])))
            # print("  . Most correlated bigrams:\n       . {}".format('\n       . '.join(trigrams[-N:])))

    def createDataSet(self):
        X_train, X_test, y_train, y_test = train_test_split(self.wines['description'], self.wines['variety'],
                                                            random_state=0)
        self.X_test = X_test
        self.X_train = X_train
        self.Y_train = y_train
        self.Y_test = y_test

    def multinomialCls(self):
        count_vect = CountVectorizer()
        X_train_counts = count_vect.fit_transform(self.X_train)
        tfidf_trasf = TfidfTransformer()
        x_train_tfidf = tfidf_trasf.fit_transform(X_train_counts)
        clf = MultinomialNB().fit(x_train_tfidf, self.Y_train)
        print clf.predict(count_vect.transform([
            "melon, pear, peach, apple, mint, coconut, eucalyptus"]))

    def multimodel(self):
        models = [
            RandomForestClassifier(n_estimators=200, max_depth=3, random_state=0),
            LinearSVC(),
            MultinomialNB(),
            LogisticRegression(random_state=0)
        ]
        CV = 5
        cv_df = pd.DataFrame(index=range(CV * len(models)))
        entries = []
        for model in models:
            model_name = model.__class__.__name__
            accuracies = cross_val_score(model, self.features, self.labels, scoring='accuracy', cv=CV)
            for fold_idx, accuracy in enumerate(accuracies):
                entries.append((model_name, fold_idx, accuracy))
        cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])

        sns.boxplot(x='model_name', y='accuracy', data=cv_df)
        sns.stripplot(x='model_name', y='accuracy', data=cv_df,
                      size=8, jitter=True, edgecolor="gray", linewidth=2)
        plt.savefig("accuracy")

    def randomForecast(self):
        model = RandomForestClassifier(n_estimators=200, max_depth=3, random_state=0)

        X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(self.features, self.labels, self.wines.index,test_size=0.25, random_state=0)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        # print y_pred

        conf_mat = confusion_matrix(y_test, y_pred)
        fig, ax = plt.subplots(figsize=(30, 20))
        sns.heatmap(conf_mat, fmt="d", annot=True, annot_kws={"fontsize": "medium"},
                    xticklabels=self.category_id_df.variety.values, yticklabels=self.category_id_df.variety.values)

        plt.ylabel('Actual')
        plt.xlabel('Predicted')
        plt.savefig("matchingRandom")

        # for predicted in self.category_id_df.category_id:
        #     for actual in self.category_id_df.category_id:
        #         if predicted != actual and conf_mat[actual, predicted] >= 5:
        #             print(
        #                 "'{}' predicted as '{}' : {} examples.".format(self.dictIdToCat[actual],
        #                                                                self.dictIdToCat[predicted],
        #                                                                conf_mat[actual, predicted]))
        #             display(self.wines.loc[indices_test[(y_test == actual) & (y_pred == predicted)]][
        #                         ['variety', 'name']])

        x = model.predict(self.tfidf.transform([
            "Alluring aromas including crushed flower, underbrush, espresso and subtle oak carry over to the palate along with raw cherry, roasted coffee bean and dried sage. It's still austere, with astringent tannins that give a drying finish. Drink 2018\\u20132030."]))
        print x
        print self.dictIdToCat[x[0]]

        print(metrics.classification_report(y_test, y_pred, target_names=self.wines['variety'].unique()))

    def logisticRegression(self):
        model = LogisticRegression(random_state=0)

        X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(self.features, self.labels,
                                                                                         self.wines.index,
                                                                                         test_size=0.25, random_state=0)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        # print y_pred

        conf_mat = confusion_matrix(y_test, y_pred)
        fig, ax = plt.subplots(figsize=(30, 20))
        sns.heatmap(conf_mat, fmt="d", annot=True, annot_kws={"fontsize": "medium"},
                    xticklabels=self.category_id_df.variety.values, yticklabels=self.category_id_df.variety.values)

        plt.ylabel('Actual')
        plt.xlabel('Predicted')
        plt.savefig("matchingLogistic")

        # for predicted in self.category_id_df.category_id:
        #     for actual in self.category_id_df.category_id:
        #         if predicted != actual and conf_mat[actual, predicted] >= 5:
        #             print(
        #                 "'{}' predicted as '{}' : {} examples.".format(self.dictIdToCat[actual],
        #                                                                self.dictIdToCat[predicted],
        #                                                                conf_mat[actual, predicted]))
        #             display(self.wines.loc[indices_test[(y_test == actual) & (y_pred == predicted)]][
        #                         ['variety', 'name']])

        x = model.predict(self.tfidf.transform([
            "Alluring aromas including crushed flower, underbrush, espresso and subtle oak carry over to the palate along with raw cherry, roasted coffee bean and dried sage. It's still austere, with astringent tannins that give a drying finish. Drink 2018\\u20132030."]))
        print x
        print self.dictIdToCat[x[0]]

        print(metrics.classification_report(y_test, y_pred, target_names=self.wines['variety'].unique()))

    def linearSVM(self):
        model = LinearSVC()
        X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(self.features, self.labels,
                                                                                         self.wines.index,
                                                                                         test_size=0.25, random_state=0)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        # print y_pred

        conf_mat = confusion_matrix(y_test, y_pred)
        fig, ax = plt.subplots(figsize=(30, 20))
        sns.heatmap(conf_mat, fmt="d", annot=True, annot_kws={"fontsize": "medium"},
                    xticklabels=self.category_id_df.variety.values, yticklabels=self.category_id_df.variety.values)

        plt.ylabel('Actual')
        plt.xlabel('Predicted')
        plt.savefig("matching")

        # for predicted in self.category_id_df.category_id:
        #     for actual in self.category_id_df.category_id:
        #         if predicted != actual and conf_mat[actual, predicted] >= 5:
        #             print(
        #                 "'{}' predicted as '{}' : {} examples.".format(self.dictIdToCat[actual],
        #                                                                self.dictIdToCat[predicted],
        #                                                                conf_mat[actual, predicted]))
        #             display(self.wines.loc[indices_test[(y_test == actual) & (y_pred == predicted)]][
        #                         ['variety', 'name']])

        x = model.predict(self.tfidf.transform([
                                                   "Alluring aromas including crushed flower, underbrush, espresso and subtle oak carry over to the palate along with raw cherry, roasted coffee bean and dried sage. It's still austere, with astringent tannins that give a drying finish. Drink 2018\\u20132030."]))
        print x
        print self.dictIdToCat[x[0]]

        print(metrics.classification_report(y_test, y_pred, target_names=self.wines['variety'].unique()))


if __name__ == '__main__':
    main()
