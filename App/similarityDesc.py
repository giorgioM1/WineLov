import csv
import json
import random
import re
import sys

import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import word_tokenize
from sklearn import cross_validation as cv
from sklearn.decomposition import PCA
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

pathChart = "/home/giorgio/Scrivania/WineLov/chart/"

reload(sys)
sys.setdefaultencoding('utf8')

pathDB = "/home/giorgio/Scrivania/WineLov/DB/"
pathCSV = "/home/giorgio/Scrivania/WineLov/CSV/"
pathModel = "/home/giorgio/Scrivania/WineLov/Model"


# similarity for the Content Based in Cold Start
# the parameters give it is: path of Dataset, the factor of reductions
# and the skipgram coef
class similarity:
    def __init__(self, pathDS, cvcoeff, skipcoef):
        header = ['id_wine', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']
        df = pd.read_csv(pathDS, sep=',', names=header, low_memory=False)
        # split dataset in train and test set
        train_data, test_data = cv.train_test_split(df, test_size=cvcoeff)
        self.max_df = 0.7
        self.min_df = 0.02
        self.training = train_data
        self.dataset = df
        self.testset = test_data
        self.matrix = None
        self.vectorized = None
        self.maxSkipGram = skipcoef
        self.getMatrix()
        # self.plot()

    def getTrainigSet(self):
        return self.training

    def setMaxSkipGram(self, max):
        self.maxSkipGram = max

    def setMaxMinDf(self, min, max):
        self.min_df = min
        self.max_df = max

    # this function allows us to transform the text, with remove the stop word, and stemmization
    def tokenize_and_stem_and_stopword(self, text):
        # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
        stems = []
        stemmer = SnowballStemmer("english")
        tokens = word_tokenize(text)
        # lower case
        tokens = [w.lower() for w in tokens]
        # remove stop words
        stop_words = set(stopwords.words('english'))
        stop_words.update(['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', "it'll"])
        words = [w for w in tokens if not w in stop_words]
        filtered_tokens = []
        # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
        for token in words:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
        return filtered_tokens
        # stems = [stemmer.stem(t) for t in filtered_tokens]
        # return stems

    # calculate tf-idf frequency
    def getMatrix(self):
        # setting vectorized of the description
        tfidf_vectorizer = TfidfVectorizer(max_df=self.max_df, min_df=self.min_df, max_features=300000,
                                           stop_words='english',
                                           use_idf=True,
                                           tokenizer=self.tokenize_and_stem_and_stopword,
                                           ngram_range=(1, self.maxSkipGram), strip_accents='unicode')

        tfidf_matrix = tfidf_vectorizer.fit_transform(self.training['description'].fillna(''))
        # print tfidf_vectorizer.vocabulary_
        self.matrix = tfidf_matrix
        self.vectorized = tfidf_vectorizer

    def tfidfMatrix(self):
        return self.matrix

    def vectorizedModel(self):
        return self.vectorized

    def exportModel(self, matrix, name):
        # export model
        joblib.dump(matrix, pathModel + name + '.pkl')
        print 'save model with name: ' + str(name)

    def importModel(self, name):
        # import model
        model = joblib.load(pathModel + name + '.pkl')
        return model

    def get_recommendations(self, Nd, n):
        Nd = self.vectorized.transform([Nd])
        cosine_sim = cosine_similarity(Nd, self.matrix)
        # Get the pairwsie similarity scores of all wines with that wine
        sim_scores = list(enumerate(cosine_sim[0]))

        # Sort the wines based on the similarity scores
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)

        # Get the scores of the n most similar wines
        sim_scores = sim_scores[1:n]
        # print sim_scores

        # Get the wine indices
        indices = [i[0] for i in sim_scores]

        # Return the top 10 most similar wines
        a = self.training['id_wine'].iloc[indices].to_json(orient='records'), self.training['name'].iloc[
            indices].to_json(orient='records'), self.training['variety'].iloc[indices].to_json(orient='records'), \
            self.training['description'].iloc[indices].to_json(orient='records')
        # print a
        return self.training['id_wine'].iloc[indices].to_json(orient='records')

    def get_recommendationsToT(self, Nd, listCFid):
        print "***", listCFid
        # trasform string to tf-idf
        Nd = self.vectorized.transform([Nd])

        cosine_sim = cosine_similarity(Nd, self.matrix)
        # Get the pairwsie similarity scores of all wines with that wine
        sim_scores = list(enumerate(cosine_sim[0]))

        # Sort the wines based on the similarity scores
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)

        # Get the scores of the 10 most similar wines
        sim_scores = sim_scores[1:400]

        # print sim_scores

        # Get the wine indices
        indices = [i[0] for i in sim_scores]

        # Return the top 10 most similar wines
        a = self.training['id_wine'].iloc[indices].to_json(orient='records')
        s = []
        js = json.loads(a)
        for i in range(0, len(json.loads(a))):
            s.append(js[i])
        a = set(listCFid)
        # print s, len(s)
        s = set(s)
        set_k = list(s.intersection(a))
        if len(set_k) != 0:
            print set_k, len(set_k)
            return True
        else:
            return False

    def get2Similarity(self, Nd1, Nd2):
        Nd1 = self.vectorized.transform([Nd1])
        Nd2 = self.vectorized.transform([Nd2])
        cosine_sim = cosine_similarity(Nd1, Nd2)
        print cosine_sim

    def plot(self):
        X = self.matrix.todense()
        print self.matrix[1]
        print self.vectorized.vocabulary_

        kmeans = KMeans(n_clusters=3).fit(X)
        labels = np.array(kmeans.labels_)

        pca = PCA(n_components=2).fit(X)
        data2D = pca.transform(X)
        plt.scatter(data2D[:, 0], data2D[:, 1], s=5, c=labels)
        # plt.show()

        centers2D = pca.transform(kmeans.cluster_centers_)

        plt.hold(True)
        plt.scatter(centers2D[:, 0], centers2D[:, 1],
                    marker='x', s=30, linewidths=5, c='r')
        # plt.show()
        plt.savefig(pathChart + 'wine' + str(random.randint(1, 1000)) + '.png')
        plt.close()


def clean(fileJDB):
    i = 0
    while True:
        try:
            if fileJDB[i]["description"] is None:
                del fileJDB[i]
            else:
                i += 1
        except:
            print "finish"
            break
    return fileJDB


def fromJsonToCsv(x):
    for row in x:
        with open(pathCSV + "DBRed.csv", "a") as fw:
            # print row['id']
            writer = csv.writer(fw, delimiter=',')
            writer.writerow([row['id'], row['name'], row['description'], row['variety'],
                             row['location'], row['type'], row['point'], row['price'], row['alchol'], row['winery']])

def main():
    pass
    # x = open(pathDB + "DBRed.json", mode="r").read()
    # jDB = json.loads(x)
    # # clean(jDB)
    # # print len(jDB)
    # # fromJsonToCsv(jDB)
    #
    # # define similarity with this dataset
    # sim = similarity(pathCSV + "DBRed.csv", 0.25, 2)
    # simwhite = similarity(pathCSV + "DBWhite.csv", 0.25, 2)
    #
    # # create matrix of tf-idf
    # # sim.getMatrix()
    # # return matrix of tf-idf
    # matrix = sim.tfidfMatrix()
    # # take the model to vectorizer a description
    # vect = sim.vectorizedModel()
    # # feature
    # terms = vect.get_feature_names()
    # # print terms
    #
    # # from a string a set of wine close to description
    # Nd1 = "exotic tobacco raspberry toasted candied fruit forest jelly peanuts floral herbal peonies cashews blueberry coffee mineral rose grilled fruit dark flint raspberry jam pie honeysuckle"
    #
    # Nd2 = "You'll find aromas of ripe berry, exotic spice, scorched earth, violet and balsamic notes on this generous fragrant red. The structured savory palate delivers crushed raspberry, succulent red cherry, star anise and tobacco alongside decisive but refined tannins. It's balanced, with fresh acidity"
    #
    # print "***********"
    #
    # sim.get2Similarity(Nd1, Nd2)
    #
    # print "***********"
    #
    # # simwhite.getMatrix()
    # vectW = simwhite.vectorizedModel()
    #
    # # Nd = vectW.transform([])
    #
    # x = simwhite.get_recommendations(
    #     "Made primarily with Vermentino, this opens with delicate scents of white spring flower, yellow stone fruit, citrus and a whiff of honey. Mirroring the nose, the fresh enveloping palate doles out dried apricot, juicy nectarine drop and a honeyed mineral note alongside tangy acidity. Hold for even more complexity. Drink through 2021.",
    #     5)
    # print x


if __name__ == '__main__':
    main()
