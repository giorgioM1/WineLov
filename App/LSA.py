import numpy
import time
import json

from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestNeighbors

import pandas as pd
from sklearn.model_selection import train_test_split

from CollaborativeFilter import pathCSV
from MultiCategoryClassification import tokenize_and_stem_and_stopword
from WineClassification import LabelClassification


class LSASimilarity:
    def __init__(self, path):
        self.df = loadDataSet(path)
        self.vectorizer = getMAtrixTFIDF()
        self.lsa = None
        self.createLSA(100)
        self.X_train = None
        self.Y_train = None
        self.X_train_lsa = None
        self.IDTRAIN = None
        self.X_test = None
        self.Y_test = None
        self.X_test_lsa = None
        self.IDTEST = None
        self.trainModel()
        self.k_NNLsa = None
        self.NNLsa = None
        self.fitOnKnn()
        self.fitNeighbours()
        self.KMeansClustering()

    def createLSA(self, vs):
        svd = TruncatedSVD(vs)
        self.lsa = make_pipeline(svd, Normalizer(copy=False))

    def trainModel(self):
        self.X_train, self.X_test, self.Y_train, self.Y_test, self.IDTRAIN, self.IDTEST = train_test_split(
            self.df['description'], self.df['name'], self.df['id_wine'], test_size=0.01, random_state=0)

        X_train_tfidf = self.vectorizer.fit_transform(self.X_train)
        self.X_train_lsa = self.lsa.fit_transform(X_train_tfidf)

        X_test_tfidf = self.vectorizer.transform(self.X_test)
        self.X_test_lsa = self.lsa.transform(X_test_tfidf)

    def fitOnKnn(self):
        self.k_NNLsa = KNeighborsClassifier(n_neighbors=5, algorithm='ball_tree')
        self.k_NNLsa.fit(self.X_train_lsa, self.Y_train)

    def predictKnn(self, des):
        Wine = self.k_NNLsa.predict([des])
        return Wine

    def fitNeighbours(self):
        self.NNLsa = NearestNeighbors(n_neighbors=5)
        self.NNLsa.fit(self.X_train_lsa, self.Y_train)

    def getNeighbours(self, des):
        # return distance and id from db beacause the population of the matrix start from 0, while the indeces from the file csv start from 1
        distance, indices = self.NNLsa.kneighbors([des], n_neighbors=20, return_distance=True)

        print distance[0][1:]
        indices = numpy.array(indices)
        print indices
        data = {}
        ids = []
        name = []
        print "NN result:"
        for p in indices[0][1:]:
            print self.Y_train.iloc[p]
            name.append(self.Y_train.iloc[p])
            # print self.X_train.iloc[p]
            ids.append(self.IDTRAIN.iloc[p])
        data['id'] = ids
        data['name'] = name
        return json.dumps(data)

    def transformDes(self, des):
        v = self.vectorizer.transform([des])
        v_lsa = self.lsa.transform(v)
        return v_lsa

    def KMeansClustering(self):
        kmeans = KMeans(n_clusters=10, random_state=0).fit(self.X_test_lsa)
        print kmeans.labels_


def loadDataSet(path):
    header = ['id_wine', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']

    df = pd.read_csv(path, header=None, sep=',', names=header, low_memory=False)
    df = df.drop(['location', 'type', 'alchol', 'winery', 'price', 'point'], axis=1)
    return df


def getMAtrixTFIDF():
    tfidf = TfidfVectorizer(max_df=0.70, max_features=300000,
                            min_df=100, stop_words='english',
                            use_idf=True,
                            norm='l2',
                            tokenizer=tokenize_and_stem_and_stopword,
                            ngram_range=(1, 3), strip_accents='unicode')
    return tfidf


def main():
    pass
    # t0 = time.time()
    # white = LSASimilarity(pathCSV + 'DBWhite.csv')
    # t1 = time.time()
    # red = LSASimilarity(pathCSV + 'DBRed.csv')
    # t2 = time.time()
    # print "time to train white: " + str(t1-t0)
    # print "time to train red: " + str(t2-t1)
    #
    # header = ['id_wine', 'name', 'description', 'variety', 'location', 'type', 'point', 'price', 'alchol', 'winery']
    # df = pd.read_csv(pathCSV + 'DB.csv', sep=',', header=None, names=header)
    #
    # LC = LabelClassification(df, "Red")
    #
    # x = "Vibrant whiffs of orange blossom and lemon peel are offset by savory bramble and earth notes in this dry, full-bodied white blend. Its juicy lemon and tangerine flavors are edged by salty, saline complexities that are just starting to develop. It's rich and textural, with a fresh, lingering finish. Hold until 2021."
    # x = white.transformDes(x)
    # #print x
    # print white.Y_train.iloc[3333]
    # print white.X_train.iloc[3333]
    # if LC.predict("Vibrant whiffs of orange blossom and lemon peel are offset by savory bramble and earth notes in this dry, full-bodied white blend. Its juicy lemon and tangerine flavors are edged by salty, saline complexities that are just starting to develop. It's rich and textural, with a fresh, lingering finish. Hold until 2021.") is False:
    #     print "White"
    #     print white.predictKnn(x[0])
    #     print white.getNeighbours(x[0])
    #
    # else:
    #     print "Red"
    #     print red.predictKnn(white.X_train_lsa[3333])
    #     red.getNeighbours(white.X_train_lsa[3333])
    #


if __name__ == '__main__':
    main()
