import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn_pandas import GridSearchCV

from WitheWineClustering import tokenize_and_stem_and_stopword
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.preprocessing import Normalizer
from sklearn.externals import joblib
from CollaborativeFilter import pathCSV
from sklearn.linear_model import SGDClassifier

categoryfood = ['crudi', 'crostacei', 'salmone', 'formaggio pastamolle', 'formaggio caprino', 'formaggio stagionato',
                'formaggio mediostagionato', 'formaggio erbolinati', 'pesce magro', 'carni bianche', 'carni rosse',
                'cacciagione', 'funghi e tartufi', 'salumi', 'legumi', 'pasta', 'marinati', 'speziati', 'riso']

PathModel = "/home/giorgio/Scrivania/WineLov/ModelK/"


class LabelClassification:
    def __init__(self, ds, n_typeCls, trainingPercent):
        self.dataset = ds
        self.food = n_typeCls
        self.trainingPercent = trainingPercent
        self.trainSet = None
        self.testSet = None
        self.labelTrain = None
        self.labelTest = None
        self.lsa = None
        self.max_df = 0.7
        self.min_df = 1
        self.matrix = None
        self.vectorized = None
        self.train_lsa = None
        self.classifier = None
        self.svm = None
        self.createDataSet()
        self.getMatrix()
        self.getLsa()
        self.LSAMatrix()
        self.classify()
        self.SVM()

    def __str__(self):
        print self

    # divide the datasets in test set and train set putting the labels
    def createDataSet(self, ):
        X_train, X_test, y_train, y_test = train_test_split(self.dataset['description'], self.dataset['food_matching'],
                                                            random_state=0, train_size=self.trainingPercent)
        self.trainSet = X_train
        self.testSet = X_test
        self.labelTrain = y_train
        self.labelTest = y_test

        self.labelTrain = [self.food in y for y in y_train]
        self.labelTest = [self.food in y for y in y_test]

    # the function creates the tf-idf matrix for the first step
    # return the vectorized model and the matrix
    def getMatrix(self):
        # setting vectorized of the description
        tfidf_vectorizer = TfidfVectorizer(max_df=self.max_df, min_df=self.min_df, max_features=300000,
                                           stop_words='english',
                                           use_idf=True,
                                           tokenizer=tokenize_and_stem_and_stopword,
                                           ngram_range=(1, 3), strip_accents='unicode')

        tfidf_matrix = tfidf_vectorizer.fit_transform(self.trainSet)

        # print tfidf_vectorizer.vocabulary_
        self.matrix = tfidf_matrix
        self.vectorized = tfidf_vectorizer

    # create the svd and Lsa object to reduce the dimension of the field
    def getLsa(self):
        svd = TruncatedSVD(100)
        self.lsa = make_pipeline(svd, Normalizer(copy=False))
        return self.lsa

    # train the lsa model with the matrix tf-idf
    def LSAMatrix(self):
        self.train_lsa = self.lsa.fit_transform(self.matrix)

    # trasform the text into lsa field
    def trasformLsa(self, text):
        x = self.vectorized.transform([text])
        x = self.lsa.transform(x)
        return x

    # define the classifier, we decided to use Knn as classifier, and we train it whit the LSA's matrix
    def classify(self):
        knn_lsa = KNeighborsClassifier(n_neighbors=5, algorithm='brute', metric='cosine')
        knn_lsa.fit(self.train_lsa, self.labelTrain)
        self.classifier = knn_lsa

    # take in input a text and predict which is your category
    def predict(self, des):
        x = self.trasformLsa(des)
        out = self.classifier.predict(x)
        print out
        if out == True:
            return True
        else:
            return False

    # create SVM classifier, fit on train data lsa
    def SVM(self):
        self.svm = SGDClassifier(loss='hinge', penalty='l2', alpha=1e-2, n_iter=5, random_state=42, n_jobs=-1, )

        self.svm.fit(self.train_lsa, self.labelTrain)

    # predicc's function for SVM
    def SVMpredict(self):
        self.svm.predict()

# definition of One VS all Classifer, if possible try to pick up from the directory else it has to recalculate
class oneVSAllClassifier:
    def __init__(self):
        self.LC_Food = None
        self.training()

    def training(self):
        header = ['id_wine', 'name', 'description', 'variety', 'food_matching']
        df = pd.read_csv(pathCSV + 'matchingFood.csv', sep=',', header=None, names=header)
        wines = df[pd.notnull(df['food_matching'])]
        # print wines.food_matching
        self.LC_Food = []
        for i in range(0, len(categoryfood)):
            try:
                print "model for " + str(categoryfood[i])
                LC = joblib.load(PathModel + categoryfood[i] + '.pkl')
                self.LC_Food.append(LC)

            except IOError:
                print "didn't find it"
                LC = LabelClassification(wines, str(categoryfood[i]), 0.85)
                joblib.dump(LC, PathModel + categoryfood[i] + '.pkl')
                self.LC_Food.append(LC)

            print("%d training examples (%d %s)" % (len(LC.labelTrain), sum(LC.labelTrain), categoryfood[i]))
            print("%d test examples (%d %s)" % (len(LC.labelTest), sum(LC.labelTest), categoryfood[i]))

    def oneVSallPredict(self, des):
        response = []
        for i in range(0, len(categoryfood)):
            p = self.LC_Food[i].predict(des)
            if p is True:
                response.append(categoryfood[i])
        return response
